m4_define([libgrassmoor_major], [0])
m4_define([libgrassmoor_minor], [2020])
m4_define([libgrassmoor_micro], [1])

m4_define([libgrassmoor_version], [libgrassmoor_major.libgrassmoor_minor.libgrassmoor_micro])

m4_define([libgrassmoor_lt_version], [0:7:0])

AC_PREREQ(2.62)
AC_INIT([libgrassmoor], libgrassmoor_version)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AM_MAINTAINER_MODE([enable])

AC_SUBST(LIBGRASSMOOR_VERSION, libgrassmoor_version)

AM_INIT_AUTOMAKE
AM_SILENT_RULES([yes])

LIBGRASSMOOR_MAJOR=libgrassmoor_major
AC_SUBST(LIBGRASSMOOR_MAJOR)

AM_INIT_AUTOMAKE([-Wno-portability])

AC_PROG_CC
AC_PROG_CXX
AC_ISC_POSIX
AC_STDC_HEADERS
AC_SEARCH_LIBS([strerror],[cposix])
AC_CHECK_FUNCS([memset])
AC_CHECK_HEADERS([fcntl.h stdlib.h string.h])

LT_INIT

LIBGRASSMOOR_PACKAGES_PUBLIC_GLIB="glib-2.0 gio-2.0"
LIBGRASSMOOR_PACKAGES_PRIVATE_GLIB="gthread-2.0 gmodule-2.0 gio-unix-2.0"

LIBGRASSMOOR_PACKAGES_PUBLIC_METATRACKER=""
LIBGRASSMOOR_PACKAGES_PRIVATE_METATRACKER="tracker-sparql-2.0 >= 1.0.0 tracker-miner-2.0 >= 1.0.0 tracker-control-2.0 >= 1.0.0"

LIBGRASSMOOR_PACKAGES_PUBLIC_CLUTTER="clutter-1.0 >= 1.18.2"
LIBGRASSMOOR_PACKAGES_PRIVATE_CLUTTER=""

LIBGRASSMOOR_PACKAGES_PUBLIC_CLUTTER_GST="clutter-gst-3.0 >= 2.99.0"
LIBGRASSMOOR_PACKAGES_PRIVATE_CLUTTER_GST=""

LIBGRASSMOOR_PACKAGES_PUBLIC_GSTREAMER="gstreamer-1.0"
LIBGRASSMOOR_PACKAGES_PRIVATE_GSTREAMER=""

LIBGRASSMOOR_PACKAGES_PUBLIC_GDK_PIXBUF=""
LIBGRASSMOOR_PACKAGES_PRIVATE_GDK_PIXBUF="gdk-pixbuf-2.0"

LIBGRASSMOOR_PACKAGES_PUBLIC_TAGLIB_C=""
LIBGRASSMOOR_PACKAGES_PRIVATE_TAGLIB_C="taglib_c >= 1.8.0"

LIBGRASSMOOR_PACKAGES_PUBLIC="$LIBGRASSMOOR_PACKAGES_PUBLIC_GLIB $LIBGRASSMOOR_PACKAGES_PUBLIC_METATRACKER $LIBGRASSMOOR_PACKAGES_PUBLIC_CLUTTER $LIBGRASSMOOR_PACKAGES_PUBLIC_CLUTTER_GST $LIBGRASSMOOR_PACKAGES_PUBLIC_GSTREAMER $LIBGRASSMOOR_PACKAGES_PUBLIC_GDK_PIXBUF $LIBGRASSMOOR_PACKAGES_PUBLIC_TAGLIB_C"
LIBGRASSMOOR_PACKAGES_PRIVATE="$LIBGRASSMOOR_PACKAGES_PRIVATE_GLIB $LIBGRASSMOOR_PACKAGES_PRIVATE_METATRACKER $LIBGRASSMOOR_PACKAGES_PRIVATE_METATRACKER $LIBGRASSMOOR_PACKAGES_PRIVATE_CLUTTER $LIBGRASSMOOR_PACKAGES_PRIVATE_CLUTTER_GST $LIBGRASSMOOR_PACKAGES_PRIVATE_GSTREAMER $LIBGRASSMOOR_PACKAGES_PRIVATE_GDK_PIXBUF $LIBGRASSMOOR_PACKAGES_PRIVATE_TAGLIB_C"

AC_SUBST([LIBGRASSMOOR_PACKAGES_PUBLIC])
AC_SUBST([LIBGRASSMOOR_PACKAGES_PRIVATE])

PKG_CHECK_MODULES([GLIB], [$LIBGRASSMOOR_PACKAGES_PUBLIC_GLIB $LIBGRASSMOOR_PACKAGES_PRIVATE_GLIB])
PKG_CHECK_MODULES([METATRACKER], [$LIBGRASSMOOR_PACKAGES_PUBLIC_METATRACKER $LIBGRASSMOOR_PACKAGES_PRIVATE_METATRACKER])
PKG_CHECK_MODULES([CLUTTER], [$LIBGRASSMOOR_PACKAGES_PUBLIC_CLUTTER $LIBGRASSMOOR_PACKAGES_PRIVATE_CLUTTER])
PKG_CHECK_MODULES([CLUTTER_GST], [$LIBGRASSMOOR_PACKAGES_PUBLIC_CLUTTER_GST $LIBGRASSMOOR_PACKAGES_PRIVATE_CLUTTER_GST])
PKG_CHECK_MODULES([GSTREAMER], [$LIBGRASSMOOR_PACKAGES_PUBLIC_GSTREAMER $LIBGRASSMOOR_PACKAGES_PRIVATE_GSTREAMER])
PKG_CHECK_MODULES([GDK_PIXBUF], [$LIBGRASSMOOR_PACKAGES_PUBLIC_GDK_PIXBUF $LIBGRASSMOOR_PACKAGES_PRIVATE_GDK_PIXBUF])
PKG_CHECK_MODULES([TAGLIB_C], [$LIBGRASSMOOR_PACKAGES_PUBLIC_TAGLIB_C $LIBGRASSMOOR_PACKAGES_PRIVATE_TAGLIB_C])

GLIB_MKENUMS=`$PKG_CONFIG --variable=glib_mkenums glib-2.0`
AC_SUBST(GLIB_MKENUMS)


#AX_PKG_CHECK_MODULES([GLIB],[glib-2.0 gio-2.0],[gthread-2.0 gmodule-2.0 gio-unix-2.0])
#AX_PKG_CHECK_MODULES([METATRACKER],[],[tracker-sparql-1.0 >= 1.0.0 tracker-miner-1.0 >= 1.0.0 tracker-control-1.0 >= 1.0.0])
#AX_PKG_CHECK_MODULES([CLUTTER],[clutter-1.0 >= 1.18.2],[])
#AX_PKG_CHECK_MODULES([CLUTTER_GST],[clutter-gst-3.0 >= 2.99.0],[])
#AX_PKG_CHECK_MODULES([GSTREAMER],[gstreamer-1.0],[])
#AX_PKG_CHECK_MODULES([GNOME_VFS],[gnome-vfs-2.0],[])
#AX_PKG_CHECK_MODULES([GDK_PIXBUF],[],[gdk-pixbuf-2.0])
#AX_PKG_CHECK_MODULES([TAGLIB_C],[],[taglib_c >= 1.8.0])

dnl Documentation
HOTDOC_CHECK([0.8], [c])

dnl check for gobject-introspection
GOBJECT_INTROSPECTION_CHECK([1.31.1])

if test "x$GCC" = "xyes"; then
    CFLAGS="$CFLAGS -g -Wall -Wno-unused-but-set-variable -Wl,--no-as-needed"
fi

# Support different levels of compiler error reporting.
# This configure flag is designed to mimic one from gnome-common,
# Defaults to "error" except for releases where it defaults to "yes"
AC_ARG_ENABLE(compile-warnings,
        AS_HELP_STRING([--enable-compile-warnings=@<:@no/minimum/yes/maximum/error@:>@],
                         [Enable different levels of compiler warnings]),,[enable_compile_warnings="yes"])

# Always pass -Werror=unknown-warning-option to get Clang to fail on bad flags.
# $1: Flag to test and append.
# $2: Flags variable to append to.
AC_DEFUN([SU_APPEND_COMPILE_FLAGS],[
 AX_APPEND_COMPILE_FLAGS([$1],[$2],[-Werror=unknown-warning-option])
])

SU_APPEND_COMPILE_FLAGS([-fno-strict-aliasing],[ERROR_CFLAGS])
SU_APPEND_COMPILE_FLAGS([-g],[ERROR_CFLAGS],[-Werror=unknown-warning-option])
SU_APPEND_COMPILE_FLAGS([-Wl,--no-as-needed],[ERROR_LDFLAGS])

AS_IF([test "$enable_compile_warnings" != "no"],[
 SU_APPEND_COMPILE_FLAGS([-Wall],[ERROR_CFLAGS])
])
AS_IF([test "$enable_compile_warnings" != "no" -a \
 "$enable_compile_warnings" != "minimum"],[
 SU_APPEND_COMPILE_FLAGS([ dnl
                 -Wextra dnl
                 -Wundef dnl
                 -Wnested-externs dnl
                 -Wwrite-strings dnl
                 -Wpointer-arith dnl
                 -Wmissing-declarations dnl
                 -Wmissing-prototypes dnl
                 -Wstrict-prototypes dnl
                 -Wredundant-decls dnl
                 -Wno-unused-parameter dnl
                 -Wno-missing-field-initializers dnl
                 -Wdeclaration-after-statement dnl
                 -Wformat=2 dnl
                 -Wold-style-definition dnl
                 -Wcast-align dnl
                 -Wformat-nonliteral dnl
                 -Wformat-security dnl
                 ],[ERROR_CFLAGS])
])

AS_IF([test "$enable_compile_warnings" = "yes" -o \
 "$enable_compile_warnings" = "maximum" -o \
 "$enable_compile_warnings" = "error"],[
 SU_APPEND_COMPILE_FLAGS([ dnl
                 -Wsign-compare dnl
                 -Wstrict-aliasing dnl
                 -Wshadow dnl
                 -Winline dnl
                 -Wpacked dnl
                 -Wmissing-format-attribute dnl
                 -Wmissing-noreturn dnl
                 -Winit-self dnl
                 -Wredundant-decls dnl
                 -Wmissing-include-dirs dnl
                 -Wunused-but-set-variable dnl
                 -Warray-bounds dnl
         ],[ERROR_CFLAGS])
])
AS_IF([test "$enable_compile_warnings" = "maximum" -o \
 "$enable_compile_warnings" = "error"],[
 SU_APPEND_COMPILE_FLAGS([ dnl
 -Wswitch-enum dnl
 -Wswitch-default dnl
 -Waggregate-return dnl
 ],[ERROR_CFLAGS])
])

AS_IF([test "$enable_compile_warnings" = "error"],[
 AX_APPEND_FLAG([-Werror],[ERROR_CFLAGS])
 SU_APPEND_COMPILE_FLAGS([ dnl
 -Wno-suggest-attribute=format dnl
 ],[ERROR_CFLAGS])
])

AC_SUBST([ERROR_CFLAGS])
AC_SUBST([ERROR_LDFLAGS])

# Installed tests
AC_ARG_ENABLE(modular_tests,
         AS_HELP_STRING([--disable-modular-tests],
         [Disable build of test programs (default: no)]),,
         [enable_modular_tests=yes])

AC_ARG_ENABLE(installed_tests,
         AS_HELP_STRING([--enable-installed-tests],
         [Install test programs (default: no)]),,
         [enable_installed_tests=no])

AM_CONDITIONAL(BUILD_MODULAR_TESTS,
         [test "$enable_modular_tests" = "yes" ||
         test "$enable_installed_tests" = "yes"])
AM_CONDITIONAL(BUILDOPT_INSTALL_TESTS, test "$enable_installed_tests" = "yes")

# code coverage
AX_CODE_COVERAGE


AC_CONFIG_FILES([
    Makefile
    docs/Makefile
    docs/reference/Makefile
    include/Makefile
    src/Makefile
    src/tracker/Makefile
    src/av-player/Makefile
    src/audio-recorder/Makefile
    src/camera-capture/Makefile
    src/camera-capture/camera-actor/Makefile
    src/camera-capture/camera-device/Makefile
    tests/Makefile
    tests/tracker/Makefile
    tests/av-player/Makefile
    tests/audio-recorder/Makefile
    libgrassmoor.pc
])

AC_OUTPUT

dnl Summary
echo ""
echo "   libgrassmoor "
echo "   --------------"
echo "        Documentation                  : ${enable_documentation}
        code coverage                  : ${enable_code_coverage}
        compiler warings               : ${enable_compile_warnings}
        Test suite                     : ${enable_modular_tests}
        Introspection                  : ${found_introspection}
"

