/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __LIBGRASSMOOR_AVPLAYER_H
#define __LIBGRASSMOOR_AVPLAYER_H

#include <clutter/clutter.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-align"
#include <clutter-gst/clutter-gst.h>
#pragma GCC diagnostic pop

/**
 * SECTION:av-player/libgrassmoor-av-player.h
 * @Title:GrassmoorAvPlayer
 * @Short_Description: grassmoor av player for audio/video playback.
 * 
 *
 * The grassmoor-av-player backend is implemented as a GObject but uses clutter-gst playback interface. Therefore it can 
 * be reused only in any clutter-based projects.This served as the backend libraries for the audio service. 
 * For audio playback it is better to use audio service because audio service takes care of audio management,
 * continuous playback and other playback funtionalities. For video playback, av player APIs should be used
 * and audio management needs to be handled by the applications by using audio manager.
 *  
 * GrassmoorAvPlayer is available since grassmoor 1.0
 */

#define GRASSMOOR_TYPE_AV_PLAYER            (grassmoor_av_player_get_type ())
#define GRASSMOOR_AV_PLAYER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GRASSMOOR_TYPE_AV_PLAYER, GrassmoorAVPlayer))
#define GRASSMOOR_AV_PLAYER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GRASSMOOR_TYPE_AV_PLAYER, GrassmoorAVPlayerClass))
#define GRASSMOOR_IS_AV_PLAYER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GRASSMOOR_TYPE_AV_PLAYER))
#define GRASSMOOR_IS_AV_PLAYER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GRASSMOOR_TYPE_AV_PLAYER))

/**
 * GrassmoorAVMode:
 * @GRASSMOOR_AV_PLAYER_MODE_INVALID: an invalid mode
 * @GRASSMOOR_AV_PLAYER_MODE_AUDIO: Audio mode
 * @GRASSMOOR_AV_PLAYER_MODE_VIDEO: Video mode creates video texture
 *
 * AV Mode to initialise AV palyer.
 */
typedef enum
{
  GRASSMOOR_AV_PLAYER_MODE_INVALID = -1,
  GRASSMOOR_AV_PLAYER_MODE_AUDIO,
  GRASSMOOR_AV_PLAYER_MODE_VIDEO
} GrassmoorAVMode;

typedef struct _GrassmoorAVPlayer GrassmoorAVPlayer;
typedef struct _GrassmoorAVPlayerClass GrassmoorAVPlayerClass;

/**
 * GrassmoorAVPlayer:
 *
 * The #GrassmoorAVPlayer struct contains only private data.
 *
 * Since: 1.0
 */
struct _GrassmoorAVPlayer
{
	/*< private >*/
	ClutterGstPlayback parentInstance;
};

/**
 * GrassmoorAVPlayerClass:
 * @video_actor_created: class handler for the #GrassmoorAVPlayer::video-actor-created signal
 * The #GrassmoorAVPlayerClass struct contains only private data.
 *
 * Since: 1.0
 *
 */
struct _GrassmoorAVPlayerClass
{
	/*< private >*/
	ClutterGstPlaybackClass parentClass;
	 /*< public >*/
  /* signals*/
  void (*video_actor_created) (GrassmoorAVPlayer *self,
                               ClutterActor *video_actor);
};

GType	grassmoor_av_player_get_type (void) G_GNUC_CONST;

GrassmoorAVPlayer * grassmoor_av_player_new (GrassmoorAVMode mode);

ClutterActor * grassmoor_av_player_get_video_actor (GrassmoorAVPlayer *self);

gboolean grassmoor_av_player_get_can_buffer (GrassmoorAVPlayer *self);

gint grassmoor_av_player_get_buffer_size (GrassmoorAVPlayer *self);
void grassmoor_av_player_set_buffer_size (GrassmoorAVPlayer *self,
                                          gint buffer_size);

#endif
