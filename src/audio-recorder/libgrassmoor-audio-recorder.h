/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __LIBGRASSMOOR_AUDIO_RECORDER_H
#define __LIBGRASSMOOR_AUDIO_RECORDER_H

#include <gst/gst.h>

/**
 * SECTION:audio-recorder/libgrassmoor-audio-recorder.h
 * @Title:GrassmoorAudioRecorder
 * @Short_Description: Grassmoor Audio Recorder to record audio stream.
 *
 * The audio recorder library is implemented as a GObject and uses gstreamer. Therefore it can be reused 
 * in any framework that uses gstreamer as the multimedia framework backend. This served as the backend 
 * libraries for the audio service for recorder interface.Recording will for now only be supported in speex 
 * format. Currently in Apertis framework, audio recording is provided by audio service 
 * recorder interface because audio service takes care of the audio management also internally.
 * Applications can use audio recorder APIs directly but in that case audio management needs to be handled by
 * the applications by using audio manager service.
 *
 * Grassmoor Audio recorder is available since libgrassmorr 1.0
 */

typedef struct _GrassmoorAudioRecorder GrassmoorAudioRecorder;
typedef struct _GrassmoorAudioRecorderClass GrassmoorAudioRecorderClass;
typedef struct _GrassmoorAudioRecorderPrivate GrassmoorAudioRecorderPrivate;

#define GRASSMOOR_TYPE_AUDIO_RECORDER            (grassmoor_audio_recorder_get_type ())
#define GRASSMOOR_AUDIO_RECORDER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GRASSMOOR_TYPE_AUDIO_RECORDER, GrassmoorAudioRecorder))
#define GRASSMOOR_AUDIO_RECORDER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GRASSMOOR_TYPE_AUDIO_RECORDER, GrassmoorAudioRecorderClass))
#define GRASSMOOR_IS_AUDIO_RECORDER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GRASSMOOR_TYPE_AUDIO_RECORDER))
#define GRASSMOOR_IS_AUDIO_RECORDER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GRASSMOOR_TYPE_AUDIO_RECORDER))

/**
 * GrassmoorAudioRecorder:
 *
 * The #GrassmoorAudioRecorder struct contains only private data.
 *
 * Since: 1.0
 */
struct _GrassmoorAudioRecorder
{
	/*< private >*/
	GstPipeline parentInstance;
	GrassmoorAudioRecorderPrivate *priv;
};

/**
 * GrassmoorAudioRecorderClass:
 * @recorder_started: class handler for the #GrassmoorAudioRecorder::recorder-started signal
 * @recorder_resumed: class handler for the #GrassmoorAudioRecorder::recorder-resumed signal
 * @recorder_paused:  class handler for the #GrassmoorAudioRecorder::recorder-paused signal
 * @recorder_completed: class handler for the #GrassmoorAudioRecorder::recorder-completed signal
 * @recorder_error: class handler for the #GrassmoorAudioRecorder::recorder-error signal
 *
 * The #GrassmoorAudioRecorderClass struct contains only private data.
 *
 * Since: 1.0
 *
 */
struct _GrassmoorAudioRecorderClass
{
	/*< private >*/
	GstPipelineClass parentClass;
	/*< public >*/
  /* signals*/
  void (*recorder_started) (GrassmoorAudioRecorder *self);
  void (*recorder_resumed) (GrassmoorAudioRecorder *self);
  void (*recorder_paused) (GrassmoorAudioRecorder *self);
  void (*recorder_completed) (GrassmoorAudioRecorder *self,
                              const gchar *file_loc);
  void (*recorder_error) (GrassmoorAudioRecorder *self,
                          const gchar *error);
};

GType	grassmoor_audio_recorder_get_type (void) G_GNUC_CONST;
GrassmoorAudioRecorder *grassmoor_audio_recorder_new(void);

void grassmoor_audio_recorder_start_recording (GrassmoorAudioRecorder *self);
void grassmoor_audio_recorder_pause_recording (GrassmoorAudioRecorder *self);
void grassmoor_audio_recorder_stop_recording(GrassmoorAudioRecorder *recorder);

void grassmoor_audio_recorder_set_file_location (GrassmoorAudioRecorder *self,
                                                 const gchar *path);

#endif
