/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* libgrassmoor-camera-capture.h */

/* GObjects representing a grassmoor camera device and camera using ClutterGst.*/

#ifndef __LIBGRASSMOOR_CAMERA_CAPTURE_H
#define __LIBGRASSMOOR_CAMERA_CAPTURE_H

#include <clutter/clutter.h>
#include <clutter-gst/clutter-gst.h>

/**
 * SECTION:camera-capture/libgrassmoor-camera-capture.h
 * @title: Camera capture
 * @short_description: GObjects representing a grassmoor camera device and camera using ClutterGst.
 */

#define GRASSMOOR_TYPE_CAMERA_DEVICE            (grassmoor_camera_device_get_type ())
#define GRASSMOOR_CAMERA_DEVICE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GRASSMOOR_TYPE_CAMERA_DEVICE, GrassmoorCameraDevice))
#define GRASSMOOR_CAMERA_DEVICE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GRASSMOOR_TYPE_CAMERA_DEVICE, GrassmoorCameraDeviceClass))
#define GRASSMOOR_IS_CAMERA_DEVICE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GRASSMOOR_TYPE_CAMERA_DEVICE))
#define GRASSMOOR_IS_CAMERA_DEVICE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GRASSMOOR_TYPE_CAMERA_DEVICE))

#define GRASSMOOR_TYPE_CAMERA            (grassmoor_camera_get_type ())
#define GRASSMOOR_CAMERA(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GRASSMOOR_TYPE_CAMERA, GrassmoorCamera))
#define GRASSMOOR_CAMERA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GRASSMOOR_TYPE_CAMERA, GrassmoorCameraClass))
#define GRASSMOOR_IS_CAMERA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GRASSMOOR_TYPE_CAMERA))
#define GRASSMOOR_IS_CAMERA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GRASSMOOR_TYPE_CAMERA))

typedef struct _GrassmoorCameraDevice GrassmoorCameraDevice;
typedef struct _GrassmoorCameraDeviceClass GrassmoorCameraDeviceClass;
typedef struct _GrassmoorCameraDevicePrivate GrassmoorCameraDevicePrivate;

typedef struct _GrassmoorCamera GrassmoorCamera;
typedef struct _GrassmoorCameraClass GrassmoorCameraClass;
typedef struct _GrassmoorCameraPrivate GrassmoorCameraPrivate;

/**
 * GrassmoorCameraDevice:
 *
 * GObject representing a camera device using ClutterGstCameraDevice.
 *
 * The #GrassmoorCameraDevice structure contains only private data and
 * should not be accessed directly.
 */
struct _GrassmoorCameraDevice
{
	/*< private >*/
	ClutterGstCameraDevice parentInstance;
};

/**
 * GrassmoorCameraDeviceClass:
 *
 * Base class for #GrassmoorCameraDevice.
 */
struct _GrassmoorCameraDeviceClass
{
	/*< private >*/
	ClutterGstCameraDeviceClass parentClass;
};

/**
 * GrassmoorCamera:
 *
 * Implementation of #GrassmoorCamera that displays camera streams
 * using ClutterGstCamera.
 *
 * The #GrassmoorCamera structure contains only private data and
 * should not be accessed directly.
 */
struct _GrassmoorCamera
{
	/*< private >*/
	ClutterGstCamera parentInstance;
};

/**
 * GrassmoorCameraClass:
 *
 * Base class for #GrassmoorCamera.
 */
struct _GrassmoorCameraClass
{
	/*< private >*/
	ClutterGstCameraClass parentClass;
};

GType	grassmoor_camera_device_get_type (void) G_GNUC_CONST;
GType	grassmoor_camera_get_type (void) G_GNUC_CONST;

GrassmoorCameraDevice* grassmoor_camera_device_new(void);
void grassmoor_camera_device_set_capture_resolution (GrassmoorCameraDevice *cameraDevice, gint width, gint height);
void grassmoor_camera_device_get_capture_resolution (GrassmoorCameraDevice *cameraDevice, gint *width, gint *height);
const GPtrArray* grassmoor_camera_device_get_supported_resolutions (GrassmoorCameraDevice *cameraDevice);
const gchar* grassmoor_camera_device_get_name (GrassmoorCameraDevice *cameraDevice);
const gchar* grassmoor_camera_device_get_node (GrassmoorCameraDevice *cameraDevice);

GrassmoorCamera* grassmoor_camera_new(void);
//GstElement *   grassmoor_camera_get_pipeline          (GrassmoorCamera  *camera);
//GstElement *   grassmoor_camera_get_camerabin         (GrassmoorCamera  *camera);

//const GPtrArray *grassmoor_camera_get_camera_devices    (GrassmoorCamera  *camera);
GrassmoorCameraDevice *grassmoor_camera_get_camera_device     (GrassmoorCamera  *camera);
gboolean       grassmoor_camera_set_camera_device
                                                              (GrassmoorCamera  *camera,
                                                               GrassmoorCameraDevice *cameraDevice);

gboolean       grassmoor_camera_supports_gamma_correction
                                                              (GrassmoorCamera  *camera);
gboolean       grassmoor_camera_get_gamma_range       (GrassmoorCamera  *camera,
                                                               gdouble                *minValue,
                                                               gdouble                *maxValue,
                                                               gdouble                *defaultValue);
gboolean       grassmoor_camera_get_gamma             (GrassmoorCamera  *camera,
                                                               gdouble                *currentValue);
gboolean       grassmoor_camera_set_gamma             (GrassmoorCamera  *camera,
                                                               gdouble                 value);

gboolean       grassmoor_camera_supports_color_balance
                                                              (GrassmoorCamera  *camera);
gboolean       grassmoor_camera_get_color_balance_property_range
                                                              (GrassmoorCamera  *camera,
                                                               const gchar            *property,
                                                               gdouble                *minValue,
                                                               gdouble                *maxValue,
                                                               gdouble                *defaultValue);
gboolean       grassmoor_camera_get_color_balance_property
                                                              (GrassmoorCamera  *camera,
                                                               const gchar            *property,
                                                               gdouble                *currentValue);
gboolean       grassmoor_camera_set_color_balance_property
                                                              (GrassmoorCamera  *camera,
                                                               const gchar            *property,
                                                               gdouble                 value);
gboolean       grassmoor_camera_get_brightness_range  (GrassmoorCamera  *camera,
                                                               gdouble                *minValue,
                                                               gdouble                *maxValue,
                                                               gdouble                *defaultValue);
gboolean       grassmoor_camera_get_brightness        (GrassmoorCamera  *camera,
                                                               gdouble                *currentValue);
gboolean       grassmoor_camera_set_brightness        (GrassmoorCamera  *camera,
                                                               gdouble                 value);
gboolean       grassmoor_camera_get_contrast_range    (GrassmoorCamera  *camera,
                                                               gdouble                *minValue,
                                                               gdouble                *maxValue,
                                                               gdouble                *defaultValue);
gboolean       grassmoor_camera_get_contrast          (GrassmoorCamera  *camera,
                                                               gdouble                *currentValue);
gboolean       grassmoor_camera_set_contrast          (GrassmoorCamera  *camera,
                                                               gdouble                 value);
gboolean       grassmoor_camera_get_saturation_range  (GrassmoorCamera  *camera,
                                                               gdouble                *minValue,
                                                               gdouble                *maxValue,
                                                               gdouble                *defaultValue);
gboolean       grassmoor_camera_get_saturation        (GrassmoorCamera  *camera,
                                                               gdouble                *currentValue);
gboolean       grassmoor_camera_set_saturation        (GrassmoorCamera  *camera,
                                                               gdouble                 value);
gboolean       grassmoor_camera_get_hue_range         (GrassmoorCamera  *camera,
                                                               gdouble                *minValue,
                                                               gdouble                *maxValue,
                                                               gdouble                *defaultValue);
gboolean       grassmoor_camera_get_hue               (GrassmoorCamera  *camera,
                                                               gdouble                *currentValue);
gboolean       grassmoor_camera_set_hue               (GrassmoorCamera  *camera,
                                                               gdouble                 value);

GstElement *   grassmoor_camera_get_filter            (GrassmoorCamera  *camera);
gboolean grassmoor_camera_set_filter (GrassmoorCamera  *camera, GstElement *filter);
gboolean grassmoor_camera_remove_filter (GrassmoorCamera *camera);
gboolean grassmoor_camera_is_ready_for_capture (GrassmoorCamera *camera);
gboolean grassmoor_camera_is_recording_video (GrassmoorCamera *camera);
void grassmoor_camera_set_video_profile (GrassmoorCamera *camera, GstEncodingProfile *profile);
gboolean grassmoor_camera_start_video_recording (GrassmoorCamera *camera, const gchar *filename);
void grassmoor_camera_stop_video_recording (GrassmoorCamera *camera);
gboolean grassmoor_camera_take_photo_pixbuf (GrassmoorCamera *camera);
gboolean grassmoor_camera_take_photo (GrassmoorCamera *camera, const gchar *filename);
void grassmoor_camera_set_photo_profile (GrassmoorCamera *camera, GstEncodingProfile *profile);
#endif
