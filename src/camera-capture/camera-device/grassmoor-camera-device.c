/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * GrassmoorCameraDevice::GrassmoorCameraDevice:
 * @Title:GrassmoorCameraDevice
 * @Short_Description: GObject representing a camera device using ClutterGst.
 *
 * #GrassmoorCameraDevice is a #GObject representing a camera device using ClutterGst.
 */

#include <glib.h>
#include "../libgrassmoor-camera-capture.h"

#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

G_DEFINE_TYPE (GrassmoorCameraDevice, grassmoor_camera_device, CLUTTER_GST_TYPE_CAMERA_DEVICE)

typedef enum   _GrassmoorCameraDeviceCodecType GrassmoorCameraDeviceCodecType;
typedef enum   _GrassmoorCameraDeviceProperty GrassmoorCameraDeviceProperty; 
typedef enum   _GrassmoorCameraDeviceSignals GrassmoorCameraDeviceSignals;

struct _GrassmoorCameraDevicePrivate
{
	gpointer ptr;
};

enum { 
  PROP_0, 
  PROP_CURR_RESOLUTION,
  PROP_SUPPORTED_RESOLUTIONS,
  PROP_NAME 
};

#define GRASSMOOR_CAMERA_DEVICE_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), GRASSMOOR_TYPE_CAMERA_DEVICE, GrassmoorCameraDevicePrivate))

static void grassmoor_camera_device_finalize (GObject *object);
static void grassmoor_camera_device_dispose (GObject *object);
static void grassmoor_camera_device_set_property (GObject *object, guint propertyId, const GValue  *value, GParamSpec *pspec);
static void grassmoor_camera_device_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec);
static void grassmoor_camera_device_class_init (GrassmoorCameraDeviceClass *klass);
static void grassmoor_camera_device_init (GrassmoorCameraDevice *cameraDevice);

static void grassmoor_camera_device_finalize (GObject *object)
{
	DEBUG ("entered");
	
	G_OBJECT_CLASS (grassmoor_camera_device_parent_class)->finalize (object);
}

static void grassmoor_camera_device_dispose (GObject *object)
{
	if(!GRASSMOOR_IS_CAMERA_DEVICE(object))
		return;
	
	DEBUG ("entered");

	G_OBJECT_CLASS (grassmoor_camera_device_parent_class)->dispose (object);
}

static void grassmoor_camera_device_set_property (GObject *object, guint propertyId, const GValue  *value, GParamSpec *pspec)
{
	DEBUG ("entered");
	switch(propertyId)
	{
		case PROP_CURR_RESOLUTION:
			{
				ClutterGstVideoResolution *videoRes = g_value_get_pointer(value);
				if( (GRASSMOOR_IS_CAMERA_DEVICE(object)) && (NULL !=videoRes) )
				{
					clutter_gst_camera_device_set_capture_resolution(CLUTTER_GST_CAMERA_DEVICE (object), videoRes->width, videoRes->height);
				}
			}
			break;

		case PROP_SUPPORTED_RESOLUTIONS:
			g_warning("Cannot set data to a readonly package \n");
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
			break;
	}
}

static void grassmoor_camera_device_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec)
{
	DEBUG ("entered");
	switch(propertyId)
	{
		case PROP_CURR_RESOLUTION:
		{
			gint width, height;
			clutter_gst_camera_device_get_capture_resolution(CLUTTER_GST_CAMERA_DEVICE(object), &width, &height);
			ClutterGstVideoResolution *resolution = g_new0(ClutterGstVideoResolution, 1);
			resolution->width = width;
			resolution->height = height;	
			g_value_set_pointer(value, resolution);
		}
		break;

		case PROP_SUPPORTED_RESOLUTIONS:
		{
			GPtrArray *resolutions = (GPtrArray *)clutter_gst_camera_device_get_supported_resolutions(CLUTTER_GST_CAMERA_DEVICE(object));
			g_value_set_pointer(value, resolutions);
		}
		break;

			/* IN case the property is not installed for this object, throw
			 * an appropriate error */
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
			break;
	}
}

static void grassmoor_camera_device_class_init (GrassmoorCameraDeviceClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;
	GParamSpec *pspec = NULL;
	DEBUG ("entered");

	/* Assign pointers to our functions */
	o_class->finalize = grassmoor_camera_device_finalize;
	o_class->dispose = grassmoor_camera_device_dispose;
	o_class->set_property = grassmoor_camera_device_set_property;
	o_class->get_property = grassmoor_camera_device_get_property;

	/* Make the private structure a part of the class */
	g_type_class_add_private (klass, sizeof (GrassmoorCameraDevicePrivate));

	/* Install the recording property (To set recording to play/pause
	 * state */
	pspec = g_param_spec_pointer("current-resolution", "Current-Resolution", "Current resolution of the camera (specific to mode (video/image). It is always a subset of the available resolutions", G_PARAM_READWRITE);
	g_object_class_install_property(o_class, PROP_CURR_RESOLUTION, pspec);
	
	pspec = g_param_spec_pointer("supported-resolutions", "Supported-Resolution", "Current resolution of the camera (specific to mode (video/image). It is always a subset of the available resolutions", G_PARAM_READABLE);
	g_object_class_install_property(o_class, PROP_SUPPORTED_RESOLUTIONS, pspec);
}

static void grassmoor_camera_device_init (GrassmoorCameraDevice *cameraDevice)
{
	DEBUG ("entered");
}

/*
 * Public symbols
 */

/**
 * grassmoor_camera_device_new:
 *
 * Creates an instance of #GrassmoorCameraDevice.
 *
 * Returns: (transfer full): #GrassmoorCameraDevice.
 */
GrassmoorCameraDevice* grassmoor_camera_device_new(void)
{
	return g_object_new(GRASSMOOR_TYPE_CAMERA_DEVICE, NULL);
}

/**
 * grassmoor_camera_device_get_node:
 * @cameraDevice: a #GrassmoorCameraDevice
 *
 * Retrieve the node (location) of the @cameraDevice.
 *
 * Returns: the device node.
 */
const gchar* grassmoor_camera_device_get_node (GrassmoorCameraDevice *cameraDevice)
{
  g_return_val_if_fail (GRASSMOOR_IS_CAMERA_DEVICE (cameraDevice), NULL);

  return clutter_gst_camera_device_get_node(CLUTTER_GST_CAMERA_DEVICE(cameraDevice));
}

/**
 * grassmoor_camera_device_get_name:
 * @cameraDevice: a #GrassmoorCameraDevice
 *
 * Retrieve the name of the @cameraDevice.
 *
 * Returns: the device name.
 */
const gchar* grassmoor_camera_device_get_name (GrassmoorCameraDevice *cameraDevice)
{
  g_return_val_if_fail (GRASSMOOR_IS_CAMERA_DEVICE (cameraDevice), NULL);

  return clutter_gst_camera_device_get_name(CLUTTER_GST_CAMERA_DEVICE(cameraDevice));
}

/**
 * grassmoor_camera_device_get_supported_resolutions:
 * @cameraDevice: a #GrassmoorCameraDevice
 *
 * Retrieve the supported resolutions of the @cameraDevice.
 *
 * Returns: (transfer none) (element-type ClutterGst.VideoResolution): an array of #ClutterGstVideoResolution with the supported resolutions.
 */
const GPtrArray* grassmoor_camera_device_get_supported_resolutions (GrassmoorCameraDevice *cameraDevice)
{
  g_return_val_if_fail (GRASSMOOR_IS_CAMERA_DEVICE (cameraDevice), NULL);

  return clutter_gst_camera_device_get_supported_resolutions (CLUTTER_GST_CAMERA_DEVICE(cameraDevice));
}

/**
 * grassmoor_camera_device_get_capture_resolution:
 * @cameraDevice: a #GrassmoorCameraDevice
 * @width: (out) (optional): Pointer to store the current capture resolution width
 * @height: (out) (optional): Pointer to store the current capture resolution height
 *
 * Retrieve the current capture resolution being used by @cameraDevice.
 */
void grassmoor_camera_device_get_capture_resolution (GrassmoorCameraDevice *cameraDevice, gint *width, gint *height)
{
  g_return_if_fail (GRASSMOOR_IS_CAMERA_DEVICE (cameraDevice));

  return clutter_gst_camera_device_get_capture_resolution (CLUTTER_GST_CAMERA_DEVICE(cameraDevice), width, height);
}

/**
 * grassmoor_camera_device_set_capture_resolution:
 * @cameraDevice: a #GrassmoorCameraDevice
 * @width: The new capture resolution width to use
 * @height: The new capture resolution height to use
 *
 * Set the capture resolution to be used by @cameraDevice.
 */
void grassmoor_camera_device_set_capture_resolution (GrassmoorCameraDevice *cameraDevice, gint width, gint height)
{
  g_return_if_fail (GRASSMOOR_IS_CAMERA_DEVICE (cameraDevice));

  return clutter_gst_camera_device_set_capture_resolution (CLUTTER_GST_CAMERA_DEVICE(cameraDevice), width, height);
}
