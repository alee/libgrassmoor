/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRACKER_INTERNAL_H__
#define __TRACKER_INTERNAL_H__
#include <libgrassmoor-tracker.h>

#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

/**
 * GrassmoorMediaInfo:
 * @enSource: #GrassmoorVolumeType for media search
 * @enFileType: #GrassmoorMediaType of required URLs
 * @url: media url
 * @title: Title field of the url
 * @sMimeType: mime type of the file
 * @iSizeInBytes: file size in bytes
 *
 * Media File Information structure for Audio, Video, Picture or Play-list
 */
struct _GrassmoorMediaInfo
{
  GrassmoorVolumeType enSource;
  GrassmoorMediaType enFileType;
  gchar *url;
  gchar *title;
  gchar *content_type;
  guint iSizeInBytes;
  union
    {
      /* Audio - Video File Properties */
      struct
        {
          guint iTrackNum;
          guint iDuration;
          gchar *artist;
          gchar *album;
          gchar *genre;
          gchar *year; /* FIXME: why not a guint? */
          gchar *album_art_url;
        } AudioVideoFileInfo;

      /* Image File Properties */
      struct
        {
          guint iHeight;
          guint iWidth;
          //gchar *       *comment; /* Currently not available */
          gchar *date;
          //GString       *author;  /* Currently not available */
          gchar *tag;
        } ImageFileInfo;

      /* Playlist File Properties */
      struct
        {
          guint iNumOfElements;
        } PlaylistFileInfo;

      /* Folder Properties */
      struct
        {
          guint iNumOfElements;
        } FolderInfo;
    } Info;
};

GrassmoorMediaInfo *tracker_taglib_get_file_info(const gchar *pUri);
gchar* tracker_taglib_get_thumbnail(const gchar *pUri);
gboolean tracker_check_thumb_exists (const gchar *pThumbPath);
const gchar* tracker_get_thumbnail_path_for_string(const gchar *string);

#endif
