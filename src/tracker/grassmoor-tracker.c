/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <libgrassmoor-tracker.h>
#include "grassmoor-tracker-internal.h"
#include <stdio.h>
#include <string.h>
#include <libtracker-sparql/tracker-sparql.h>
#include <libtracker-control/tracker-control.h>
#include <sys/time.h>
#include <gio/gio.h>

//#define THUMBNAILER_SERVICE        "org.freedesktop.thumbnails.Thumbnailer1"
//#define THUMBNAILER_PATH           "/org/freedesktop/thumbnails/Thumbnailer1"
//#define THUMBNAILER_INTERFACE      "org.freedesktop.thumbnails.Thumbnailer1"

#define TRACKER_MINER_FILES "org.freedesktop.Tracker1.Miner.Files"

typedef enum _enCopyType enCopyType;
enum _enCopyType
{
	TRACKER_COPY_TO_STRUCTURE,
	TRACKER_COPY_TO_ARRAY,
};

static const gchar *FileTypeOntology[GRASSMOOR_TRACKER_MEDIA_TYPE_COUNT] =
{
		"nmm:MusicPiece",
		"nmm:Video",
		"nfo:Image",
		"nmm:Playlist",
		"nfo:Document",
		"nfo:Folder",
		"nfo:FileDataObject",
		"nfo:FileDataObject",
		"nie:DataObject"
};

static const gchar *InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_COUNT] =
{
		"nie:url(?file)",
		"?source",
		"?mime",
		"?size",
		"?title",
		"?performer",
		"?album",
		"?genre",
		"?year",
		"?duration",
		"?trackno",
		"?height",
		"?width",
		"?tag",
		"?date",
		"?comment"
		"?NumOfElements"
		" "
};

static const gchar *InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_COUNT] =
{
		" ",
		"?volume tracker:isRemovable ?source.",
		"?file nie:mimeType ?mime.",
		"?file nfo:fileSize ?size.",
		"OPTIONAL { ?file nie:title ?title  }.",
		"OPTIONAL { ?file nmm:performer [ nmm:artistName ?performer ] }.",
		"OPTIONAL { ?file nmm:musicAlbum [ nie:title ?album ] }.",
		"OPTIONAL { ?file nfo:genre ?genre }.",
		"OPTIONAL { ?file nie:contentCreated ?year }.",
		"?file nfo:duration ?duration.",
		"OPTIONAL { ?file nmm:trackNumber ?trackno  }.",
		"OPTIONAL { ?file nfo:height ?height  }.",
		"OPTIONAL { ?file nfo:width ?width  }.",
		"OPTIONAL { ?file nao:hasTag [nao:prefLabel ?tag] }.",
		"OPTIONAL { ?file nfo:fileLastModified ?date }.",
		"OPTIONAL { ?file nie:comment ?comment }.",
		"OPTIONAL { ?file nfo:entryCounter ?NumOfElements }.",
		" "
};

#define TRACKER_IS_AUDIO_VIDEO(fileType) ((fileType == GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO) || (fileType == GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO))
#define TRACKER_IS_PICTURE(fileType) (fileType == GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO)
#define TRACKER_IS_NULL(fileType) (fileType == NULL)
#define TRACKER_IS_REMOVABLE(volume) (volume == GRASSMOOR_TRACKER_VOLUME_REMOVABLE)

struct _GrassmoorTracker
{
  GObject parent_instance;

  TrackerMinerManager *miner_mgr;
  TrackerSparqlConnection *conn;

  guint32 cookie;
};

G_DEFINE_TYPE (GrassmoorTracker, grassmoor_tracker, G_TYPE_OBJECT);

//static GDBusProxy *thumbnailer_service = NULL;
//static GDBusConnection *connection = NULL;

//static void(*thumbnail_creation) (const gchar** pFileName);

/******************************************************************************
 * tracker_copy_data_to_mediainfo_struct:
 *
 *****************************************************************************/
static GrassmoorMediaInfo *tracker_copy_data_to_mediainfo_struct(TrackerSparqlCursor *cursor, GrassmoorMediaType enFileType)
{
	const char* ResultNum = NULL;
	if(cursor)
	{
		GrassmoorMediaInfo *pResultStructure = g_new0 (GrassmoorMediaInfo, 1);
		pResultStructure->title = NULL;
		pResultStructure->content_type = NULL;
		pResultStructure->enFileType = enFileType;
		pResultStructure->url = g_strdup (tracker_sparql_cursor_get_string (cursor, 0, NULL));
		if(!g_ascii_strncasecmp ("false", tracker_sparql_cursor_get_string (cursor, 1, NULL), sizeof "false"))
		{
			pResultStructure->enSource = GRASSMOOR_TRACKER_VOLUME_LOCAL;
		}
		else
		{
			pResultStructure->enSource = GRASSMOOR_TRACKER_VOLUME_REMOVABLE;
		}

		pResultStructure->content_type = g_strdup (tracker_sparql_cursor_get_string (cursor, 2, NULL));

		ResultNum = tracker_sparql_cursor_get_string (cursor, 3, NULL);
		if(ResultNum && *ResultNum)
		{
			pResultStructure->iSizeInBytes = g_ascii_strtod (ResultNum, NULL);
		}


		if(TRACKER_IS_AUDIO_VIDEO(enFileType))
		{
			pResultStructure->title = g_strdup (tracker_sparql_cursor_get_string (cursor, 5, NULL));
			pResultStructure->Info.AudioVideoFileInfo.artist = g_strdup (tracker_sparql_cursor_get_string (cursor, 6, NULL));
			pResultStructure->Info.AudioVideoFileInfo.album = g_strdup (tracker_sparql_cursor_get_string (cursor, 7, NULL));
			pResultStructure->Info.AudioVideoFileInfo.genre = g_strdup (tracker_sparql_cursor_get_string (cursor, 8, NULL));
			pResultStructure->Info.AudioVideoFileInfo.year = g_strndup (tracker_sparql_cursor_get_string (cursor, 9, NULL), 4);
			ResultNum = tracker_sparql_cursor_get_string (cursor, 4, NULL);
			if(ResultNum && *ResultNum)
			{
				pResultStructure->Info.AudioVideoFileInfo.iTrackNum = g_ascii_strtod (ResultNum, NULL);
			}
			ResultNum = tracker_sparql_cursor_get_string (cursor, 10, NULL);
			if(ResultNum && *ResultNum)
			{
				pResultStructure->Info.AudioVideoFileInfo.iDuration = g_ascii_strtod (ResultNum, NULL);
			}
		}
		else if(TRACKER_IS_PICTURE(enFileType))
		{
			if(pResultStructure->url)
			{
				gchar* imageFilePath = NULL;
				imageFilePath = g_filename_from_uri (pResultStructure->url, NULL, NULL);
				if(imageFilePath)
				{
					pResultStructure->title = g_path_get_basename(imageFilePath);
					g_free(imageFilePath);
				}
			}
			ResultNum = tracker_sparql_cursor_get_string (cursor, 4, NULL);
			if(ResultNum && *ResultNum)
			{
				pResultStructure->Info.ImageFileInfo.iHeight = g_ascii_strtod (ResultNum, NULL);
			}
			else
				pResultStructure->Info.ImageFileInfo.iHeight = 0;

			ResultNum = tracker_sparql_cursor_get_string (cursor, 5, NULL);
			if(ResultNum && *ResultNum)
			{
				pResultStructure->Info.ImageFileInfo.iWidth = g_ascii_strtod (ResultNum, NULL);
			}
			else
				pResultStructure->Info.ImageFileInfo.iWidth = 0;
			pResultStructure->Info.ImageFileInfo.tag = g_strdup (tracker_sparql_cursor_get_string (cursor, 6, NULL));
			pResultStructure->Info.ImageFileInfo.date = g_strdup (tracker_sparql_cursor_get_string (cursor, 7, NULL));
		}
		return pResultStructure;
	}
	return NULL;
}

/******************************************************************************
 * tracker_copy_from_cursor:
 *
 *
 *****************************************************************************/
static guint tracker_copy_from_cursor(TrackerSparqlCursor *cursor, GPtrArray *pResult, enCopyType CopyTo, GrassmoorMediaType enFileType, gint search_limit)
{
	guint count = 0;
	while (tracker_sparql_cursor_next (cursor, NULL, NULL))
	{
		if(CopyTo == TRACKER_COPY_TO_ARRAY)
		{
			const gchar *result;
			result = tracker_sparql_cursor_get_string (cursor, 0, NULL);
			g_ptr_array_add (pResult, g_strdup (result));
		}
		else if(CopyTo == TRACKER_COPY_TO_STRUCTURE)
		{
			GrassmoorMediaInfo *pResultStructure = NULL;
			pResultStructure = tracker_copy_data_to_mediainfo_struct(cursor, enFileType);
			g_ptr_array_add (pResult, (gpointer) pResultStructure);
		}
		count++;
	}

	if (count >= search_limit)
	{
		DEBUG (" Exceeding limit: %d", search_limit);
	}
	else
	{
		DEBUG ("Total Entries: %d", count);
	}
	return count;
}

/**
 * grassmoor_tracker_get_url_list:
 * @self: a #GrassmoorTracker
 * @volume: #GrassmoorVolumeType
 * @volume_mount_point: Mount point in case of removable media as @volume, else NULL
 * @fileType: #GrassmoorMediaType of required URLs
 * @enSortingType: #GrassmoorMediaInfoType for sorting of URL List
 * @offset: Offset for URL list
 * @iLimit: Limit for total URLs in list
 * @error: #GError for error reporting
 *
 * Gives list of Audio, Video, Photo, play-list File URLs for a particular volume
 *
 * Returns: (nullable) (element-type utf8) (transfer container): a #GPtrArray of URLs of requested @fileType if found,
 * %NULL otherwise. On error, %NULL is returned and the error is set
 * accordingly.
 *
 * #Sample Code:
 * |[<!-- language="C" -->
 * void print_array(GPtrArray *pResultPointer)
 * {
 * 	 gint iCount = 0;
 * 	 if(pResultPointer)
 * 	 {
 * 	      for(iCount = 0; iCount < pResultPointer->len; iCount++)
 *         {
 *            const gchar *url = g_ptr_array_index (pResultPointer, iCount);
 *            g_print("audio url = %s\n", url);
 *         }
 *         g_ptr_array_free(pResultPointer, TRUE);
 *	  }
 * 	  g_print ("\nTotal elements: %d\n", iCount);
 *	}
 * 
 * GrassmoorTracker *tracker;
 *
 * tracker = grassmoor_tracker_new ();
 *
 * GPtrArray *pResultPointer = NULL;
 * pResultPointer = grassmoor_tracker_get_url_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL,
 * 			NULL,
 * 			GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO,
 *                  	GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST,
 *                      GRASSMOOR_TRACKER_ORDER_TYPE_DESC, 0, 1000, NULL);
 *
 * g_object_unref (tracker);
 * ]|
 * 
 */
GPtrArray *
grassmoor_tracker_get_url_list (GrassmoorTracker *self,
                                GrassmoorVolumeType volume,
                                const gchar *volume_mount_point,
                                GrassmoorMediaType fileType,
                                GrassmoorMediaInfoType enSortingType,
                                GrassmoorQueryOrderType queryOrder,
                                gint offset, gint iLimit,
                                GError **error)
{
		GError *err = NULL;
		TrackerSparqlCursor *cursor;
		gchar *query;
		gchar *mountPoint = NULL;
		const gchar *show_all_str;
		gboolean show_all = FALSE;
		show_all_str = show_all ? "" : "?file tracker:available true . ";

		if(volume_mount_point)
			mountPoint = g_strdup_printf("\'%s\'.", volume_mount_point);
		else
			mountPoint = g_strdup("");

		query = g_strdup_printf ("SELECT %s "
				"WHERE { "
				"?volume a tracker:Volume ; "
				"%s %s %s %s"
				"  ?file a %s ; "
				" nie:dataSource ?volume ."
				"  %s"
				"} "
				"ORDER BY %s(%s) "
				"OFFSET %d "
				"LIMIT %d",
				InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_URL],
				TRACKER_IS_REMOVABLE(volume) ? "tracker:isRemovable true" : "tracker:isRemovable false",
						(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? "; tracker:mountPoint ?m." : ".",
								(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? "?m a nfo:Folder; nie:url " : "",
										(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? mountPoint : "",
												FileTypeOntology[fileType],
												show_all_str,
												queryOrder?"DESC":"ASC",
														InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_URL],
														offset,
														iLimit
		);

		DEBUG ("Query  generated = %s", query);
		cursor = tracker_sparql_connection_query (self->conn, query, NULL, &err);
		g_free (query);
		g_free (mountPoint);

		if (err)
		{
                  WARNING ("%s: code %d: %s", g_quark_to_string (err->domain), err->code, err->message);
                  g_propagate_error (error, err);
                  if (cursor)
                    g_object_unref (cursor);
                  return NULL;
		}
		else
		{
			GPtrArray *pResultPointer = g_ptr_array_new_with_free_func (g_free);
			tracker_copy_from_cursor(cursor, pResultPointer, TRACKER_COPY_TO_ARRAY, fileType, iLimit);
			if(cursor) g_object_unref (cursor);
			return pResultPointer;
		}
	return NULL;
}
/******************************************************************************
 * tracker_build_media_file_info_query:
 *
 *
 *****************************************************************************/
static gchar *tracker_build_media_file_info_query(const gchar *FileUrl, GrassmoorMediaType enFileType)
{
	gchar *query;
	const gchar *show_all_str;
	gboolean show_all = FALSE;
	show_all_str = show_all ? "" : "?file tracker:available true . ";
	query = g_strdup_printf ("SELECT %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s "
			"WHERE { ?file nie:url \"%s\" . ?volume a tracker:Volume . %s %s %s %s %s %s %s %s %s %s %s %s %s %s } ",
			InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_URL],
			InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_SOURCE],
			InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_MIME],
			InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_SIZE],
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_TRACK_NUM] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_TITLE] : "",
					TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST] : "",
							TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM] : "",
									TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_GENRE] : "",
											TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_YEAR] : "",
															TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_DURATION] : "",
																	TRACKER_IS_PICTURE(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_HEIGHT] : "",
																			TRACKER_IS_PICTURE(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_WIDTH] : "",
																					TRACKER_IS_PICTURE(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_TAG] : "",
																							TRACKER_IS_PICTURE(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_DATE] : "",
																									FileUrl,
																									InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_SOURCE],
																									InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_MIME],
																									InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_SIZE],
				TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_TRACK_NUM] : "",
																									//show_all_str,
	TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_TITLE] : "",
TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST] : "",
TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM] : "",
TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_GENRE] : "",
TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_YEAR] : "",
TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_DURATION] : "",
																																							TRACKER_IS_PICTURE(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_HEIGHT] : "",
																																									TRACKER_IS_PICTURE(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_WIDTH] : "",
																																											TRACKER_IS_PICTURE(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_TAG] : "",
																																													TRACKER_IS_PICTURE(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_DATE] : "" );
	return query;
}


/**
 * grassmoor_tracker_get_media_file_info:
 * @self: a #GrassmoorTracker
 * @FileUrl: File URL whose info is requested
 * @enFileType: #GrassmoorMediaType type of media file
 * @error: #GError for error reporting
 *
 * Gives detailed info for a particular Audio, Video, Photo, play-list File
 *
 * Returns: (transfer full) (nullable): a #GrassmoorMediaInfo if found, %NULL otherwise. On error, %NULL is
 * returned and the error is set accordingly.
 **/
GrassmoorMediaInfo *
grassmoor_tracker_get_media_file_info (GrassmoorTracker *self,
                                       const gchar *FileUrl,
                                       GrassmoorMediaType enFileType,
                                       GError **error)
{
	TrackerSparqlCursor *cursor;
	gchar *query = NULL;
	GrassmoorMediaInfo *Result = NULL;

	/* if file type is unknown, use taglib api to get meta data */
	if(enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_UNKNOWN)
	{
		Result = tracker_taglib_get_file_info(FileUrl);		
		return Result;	
	}
		GError *err = NULL;

		query = tracker_build_media_file_info_query(FileUrl, enFileType);
		cursor = tracker_sparql_connection_query (self->conn, query, NULL, &err);
		g_free (query);
		if (err)
		{
                  WARNING ("%s: code %d: %s", g_quark_to_string (err->domain), err->code, err->message);
				  if (cursor)
                    g_object_unref (cursor);
                  /* if error to get meta data using tracker, try with taglib api to get meta data */	
                  Result = tracker_taglib_get_file_info(FileUrl);

                  if (Result)
                    g_error_free (err);
                  else
                    g_propagate_error (error, err);

                  return Result;
		}
		else if(cursor)
		{
			if (tracker_sparql_cursor_next (cursor, NULL, NULL))
			{
				Result = tracker_copy_data_to_mediainfo_struct(cursor, enFileType);
			}
			if(cursor) g_object_unref (cursor);
			/* sometimes if tracker fails , try with taglib api to get meta data */ 
			if(NULL == Result)
			{
				DEBUG ("Tracker fails to get metadata for %s. Trying using taglib apis", FileUrl);
				Result = tracker_taglib_get_file_info(FileUrl);
			}
			return Result;
		}
	return NULL;
}

/********************************************************************************
 * tracker_build_media_info_list_query:
 *
 *
 ********************************************************************************/
static gchar *
tracker_build_media_info_list_query (GrassmoorVolumeType volume,
                                     const gchar *volume_mount_point,
                                     GrassmoorMediaType enFileType,
                                     GrassmoorMediaInfoType enSortingType,
                                     GrassmoorQueryOrderType queryOrder,
                                     gint ioffset,
                                     gint iLimit)
{
	gchar *query;
	const gchar *show_all_str;
	gboolean show_all = FALSE;
	gchar *mountPoint = NULL;

	show_all_str = show_all ? "" : "?file tracker:available true . ";

	if(volume_mount_point)
		mountPoint = g_strdup_printf("\'%s\'.", volume_mount_point);
	else
		mountPoint = g_strdup("");

	query = g_strdup_printf ("SELECT %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s "
			"WHERE { ?volume a tracker:Volume ; %s %s %s %s"
			"?file a %s ; nie:dataSource ?volume . %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s} "
			"ORDER BY %s(%s) %s(?title) "
			"OFFSET %d "
			"LIMIT %d",
			InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_URL],
			InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_SOURCE],
			InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_MIME],
			InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_SIZE],
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_TRACK_NUM] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_TITLE] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_GENRE] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_YEAR] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_DURATION] : "",
			TRACKER_IS_PICTURE(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_HEIGHT] : "",
			TRACKER_IS_PICTURE(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_WIDTH] : "",
			TRACKER_IS_PICTURE(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_TAG] : "",
			TRACKER_IS_PICTURE(enFileType) ? InfoVariables[GRASSMOOR_TRACKER_MEDIA_INFO_DATE] : "",
			TRACKER_IS_REMOVABLE(volume) ? "tracker:isRemovable true" : "tracker:isRemovable false",
			(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? "; tracker:mountPoint ?m." : ".",
			(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? "?m a nfo:Folder; nie:url " : "",
			(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? mountPoint : "",
			FileTypeOntology[enFileType],
			InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_SOURCE],
			InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_MIME],
			InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_SIZE],
			show_all_str,
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_TRACK_NUM] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_TITLE] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_DURATION] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_GENRE] : "",
			TRACKER_IS_AUDIO_VIDEO(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_YEAR] : "",
																																				TRACKER_IS_PICTURE(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_HEIGHT] : "",
																																																	TRACKER_IS_PICTURE(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_WIDTH] : "",
																																																			TRACKER_IS_PICTURE(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_TAG] : "",
																																																					TRACKER_IS_PICTURE(enFileType) ? InfoVariablesDec[GRASSMOOR_TRACKER_MEDIA_INFO_DATE] : "",
																																																							queryOrder?"DESC":"ASC",
																																																									InfoVariables[enSortingType],
																																																									queryOrder?"DESC":"ASC",
																																																											ioffset, iLimit);

	g_free(mountPoint);
	return query;
}
/**
 * grassmoor_tracker_get_media_info_list:
 * @self: a #GrassmoorTracker
 * @volume: #GrassmoorVolumeType
 * @volume_mount_point: Mount point in case of removable media as @volume, else NULL
 * @enFileType: #GrassmoorMediaType of required URLs
 * @enSortingType: #GrassmoorMediaInfoType for sorting of URL List
 * @ioffset: Offset for URL list
 * @iLimit: Limit for total URLs in list
 * @error: #GError for error reporting
 *
 * Gives list of detailed GrassmoorMediaInfo for Audio, Video, Photo, play-list Files for a particular volume
 *
 * Returns: (nullable) (element-type GrassmoorMediaInfo) (transfer container): a #GPtrArray of requested #GrassmoorMediaInfo structures for
 * requested #enFileType if found, %NULL otherwise. On error, %NULL is returned
 * and the error is set accordingly.
 **/
GPtrArray *
grassmoor_tracker_get_media_info_list (GrassmoorTracker *self,
                                       GrassmoorVolumeType volume,
                                       const gchar *volume_mount_point,
                                       GrassmoorMediaType enFileType,
                                       GrassmoorMediaInfoType enSortingType,
                                       GrassmoorQueryOrderType queryOrder,
                                       gint ioffset,
                                       gint iLimit,
                                       GError **error)
{
		GError *err = NULL;
		TrackerSparqlCursor *cursor;
		gchar *query;
		query = tracker_build_media_info_list_query(volume, volume_mount_point, enFileType, enSortingType, queryOrder,ioffset, iLimit);
		cursor = tracker_sparql_connection_query (self->conn, query, NULL, &err);
		g_free (query);

                if (err)
                  {
                    WARNING ("%s: code %d: %s", g_quark_to_string (err->domain), err->code, err->message);
					g_propagate_error (error, err);
                    if (cursor)
                      g_object_unref (cursor);
                    return NULL;
		}
		else
		{
			GPtrArray *pResultPointer = g_ptr_array_new_with_free_func ((GDestroyNotify) grassmoor_media_info_free);
			tracker_copy_from_cursor(cursor, pResultPointer, TRACKER_COPY_TO_STRUCTURE, enFileType, iLimit);
			if(cursor) g_object_unref (cursor);
			return pResultPointer;
		}
	return NULL;
}

/**
 * grassmoor_tracker_get_meta_list:
 * @self: a #GrassmoorTracker
 * @volume: #GrassmoorVolumeType
 * @volume_mount_point: Mount point in case of removable media as @volume, else NULL
 * @enInfoType: #GrassmoorMediaInfoType of required list
 * @ioffset: Offset for URL list
 * @iLimit: Limit for total URLs in list
 * @error: #GError for error reporting
 *
 * Returns: (nullable) (element-type utf8) (transfer container): a #GPtrArray of strings requested #enInfoType if found,
 * %NULL otherwise. On error, %NULL is returned and the error is set
 * accordingly.
 *
 * Gives List of Artists, Albums, Genres, Songs for a particular @volume
 **/
GPtrArray *
grassmoor_tracker_get_meta_list (GrassmoorTracker *self,
                                 GrassmoorVolumeType volume,
                                 const gchar *volume_mount_point,
                                 GrassmoorMediaInfoType enInfoType,
                                 gint ioffset,
                                 gint iLimit,
                                 GError **error)
{
		GError *err = NULL;
		TrackerSparqlCursor *cursor;
		gchar *query;
		const gchar *show_all_str;
		gchar *mountPoint = NULL;

		gboolean show_all = FALSE;
		show_all_str = show_all ? "" : "?file tracker:available true . ";

		if (volume_mount_point)
			mountPoint = g_strdup_printf("\'%s\'.", volume_mount_point);
		else
			mountPoint = g_strdup("");

		query = g_strdup_printf ("SELECT DISTINCT %s WHERE { ?volume a tracker:Volume ; %s %s %s %s ?file a nmm:MusicPiece; nie:dataSource ?volume ."
				"  %s"
				"  %s"
				"} "
				"ORDER BY ASC(%s) "
				"OFFSET %d "
				"LIMIT %d",
				InfoVariables[enInfoType],
				TRACKER_IS_REMOVABLE(volume) ? "tracker:isRemovable true" : "tracker:isRemovable false",
						(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? "; tracker:mountPoint ?m." : ".",
								(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? "?m a nfo:Folder; nie:url " : "",
										(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? mountPoint : "",
												InfoVariablesDec[enInfoType],
												show_all_str,
												InfoVariables[enInfoType],
												ioffset,
												iLimit
		);

		DEBUG ("Query = %s", query);
		cursor = tracker_sparql_connection_query (self->conn, query, NULL, &err);
		g_free (query);
		g_free(mountPoint);

		if (err)
		{
                  WARNING ("%s: code %d: %s", g_quark_to_string (err->domain), err->code, err->message);
                  g_propagate_error (error, err);
                  if (cursor)
                    g_object_unref (cursor);
                  return NULL;
		}
		else
		{
			GPtrArray *pResultPointer = g_ptr_array_new_with_free_func (g_free);
			tracker_copy_from_cursor(cursor, pResultPointer, TRACKER_COPY_TO_ARRAY, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, iLimit);
			if(cursor) g_object_unref (cursor);
			return pResultPointer;
		}
	return NULL;
}

/**
 * grassmoor_tracker_get_meta_list_for:
 * @self: a #GrassmoorTracker
 * @volume: #GrassmoorVolumeType
 * @volume_mount_point: Mount point in case of removable media as @volume, else NULL
 * @enInputInfoType: #GrassmoorMediaInfoType for which info is required
 * @sInput: String of input for which info is required
 * @enOutputInfoType: #GrassmoorMediaInfoType for the output list
 * @ioffset: Offset for URL list
 * @iLimit: Limit for total URLs in list
 * @error: #GError for error reporting
 *
 * Gives required info list for particular Artist, Album, or Genre
 *
 * Returns: (nullable) (element-type utf8) (transfer container): a #GPtrArray of strings requested @enInputInfoType if
 * found, %NULL otherwise, On error, %NULL is returned and the error is set accordingly.
 **/
GPtrArray *
grassmoor_tracker_get_meta_list_for (GrassmoorTracker *self,
                                     GrassmoorVolumeType volume,
                                     const gchar *volume_mount_point,
                                     GrassmoorMediaInfoType enInputInfoType,
                                     const gchar *sInput,
                                     GrassmoorMediaInfoType enOutputInfoType,
                                     gint ioffset,
                                     gint iLimit,
                                     GError **error)
{
		GError *err = NULL;
		TrackerSparqlCursor *cursor;
		gchar *query;
		const gchar *show_all_str;
		gchar *mountPoint = NULL;

		gboolean show_all = FALSE;
		show_all_str = show_all ? "" : "?file tracker:available true . ";
		if (volume_mount_point)
			mountPoint = g_strdup_printf("\'%s\'.", volume_mount_point);
		else
			mountPoint = g_strdup("");

		query = g_strdup_printf ("SELECT %s WHERE { ?volume a tracker:Volume ; %s %s %s %s ?file a nmm:MusicPiece; nie:dataSource ?volume ."
				"  FILTER regex(%s, \"%s\")."
				"  %s"
				"  %s"
				"  %s"
				"} "
				"ORDER BY ASC(%s) "
				"OFFSET %d "
				"LIMIT %d",
				InfoVariables[enOutputInfoType],
				TRACKER_IS_REMOVABLE(volume) ? "tracker:isRemovable true" : "tracker:isRemovable false",
						(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? "; tracker:mountPoint ?m." : ".",
								(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? "?m a nfo:Folder; nie:url " : "",
										(volume_mount_point && TRACKER_IS_REMOVABLE(volume)) ? mountPoint : "",
												InfoVariables[enInputInfoType],
												sInput,
												InfoVariablesDec[enOutputInfoType],
												InfoVariablesDec[enInputInfoType],
												show_all_str,
												InfoVariables[enOutputInfoType],
												ioffset,
												iLimit
		);

		DEBUG ("Query = %s", query);
		cursor = tracker_sparql_connection_query (self->conn, query, NULL, &err);
		g_free (query);
		g_free (mountPoint);

		if (err)
		{
			WARNING ("%s: code %d: %s", g_quark_to_string (err->domain), err->code, err->message);
			g_propagate_error (error, err);
			if (cursor)
				g_object_unref (cursor);
			return NULL;
		}
		else
		{
			GPtrArray *pResultPointer = g_ptr_array_new_with_free_func (g_free);
			tracker_copy_from_cursor(cursor, pResultPointer, TRACKER_COPY_TO_ARRAY, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, iLimit);
			if(cursor) g_object_unref (cursor);
			return pResultPointer;
		}
	return NULL;
}

/**
 * grassmoor_tracker_get_url_list_for_playlist:
 * @self: a #GrassmoorTracker
 * @PlaylistPath: Path for input play-list
 * @ioffset: Offset for URL list
 * @iLimit: Limit for total URLs in list
 * @error: #GError for error reporting
 *
 * Gives list of urls for a particular Playlist
 *
 * Returns: (nullable) (element-type utf8) (transfer container): a #GPtrArray of strings for URLs of files from Play-list if found,
 * %NULL otherwise. On error, %NULL is returned and the error is set
 * accordingly.
 **/
GPtrArray *
grassmoor_tracker_get_url_list_for_playlist (GrassmoorTracker *self,
                                             const gchar *PlaylistPath,
                                             gint ioffset,
                                             gint iLimit,
                                             GError **error)
{
		GError *err = NULL;
		TrackerSparqlCursor *cursor;
		gchar *query;
		query = g_strdup_printf ("SELECT nfo:entryUrl(?entry) nfo:listPosition(?entry) "
				" WHERE { "
				" ?playlist nie:url \"%s\"."
				" ?playlist nfo:hasMediaFileListEntry ?entry."
				" } "
				" ORDER BY ASC(nfo:listPosition) "
				" OFFSET %d "
				" LIMIT %d",
				PlaylistPath,
				ioffset,
				iLimit
		);
		
		DEBUG ("Query = %s", query);
		cursor = tracker_sparql_connection_query (self->conn, query, NULL, &err);
		g_free (query);

		if (err)
		{
			WARNING ("%s: code %d: %s", g_quark_to_string (err->domain), err->code, err->message);
			g_propagate_error (error, err);
			if (cursor)
				g_object_unref (cursor);
			return NULL;
		}
		else
		{
			GPtrArray *pResultPointer = g_ptr_array_new_with_free_func (g_free);
			tracker_copy_from_cursor(cursor, pResultPointer, TRACKER_COPY_TO_ARRAY, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, iLimit);
			if(cursor) g_object_unref (cursor);
			return pResultPointer;
		}
	return NULL;
}

/**
 * grassmoor_tracker_pause_indexing:
 * @self: a #GrassmoorTracker
 *
 * Pauses tracker indexing if in progress
 *
 * Returns: %TRUE if the operation succeeded, else %FALSE.
 **/
gboolean
grassmoor_tracker_pause_indexing (GrassmoorTracker *self)
{
  gboolean result;

  result = tracker_miner_manager_pause (self->miner_mgr, TRACKER_MINER_FILES,
                                        "User Pause", &self->cookie);

  DEBUG ("tracker-miner-fs pause %s", result ? "Succesfull" : "Failed");
  return result;
}

/**
 * grassmoor_tracker_start_indexing:
 * @self: a #GrassmoorTracker
 *
 * starts tracker indexing if not complete
 *
 * Returns: %TRUE if the operation succeeded, else %FALSE.
 **/
gboolean
grassmoor_tracker_start_indexing (GrassmoorTracker *self)
{
  gboolean result;

  result = tracker_miner_manager_resume (self->miner_mgr, TRACKER_MINER_FILES,
                                         self->cookie);
  DEBUG ("tracker-miner-fs resume %s", result ? "Succesfull":"Failed");
  return result;
}

/* Mode flags to pass to tracker. This list is incomplete. */
typedef enum {
	GRASSMOOR_TRACKER_CONTROL_HARD_RESET,
	GRASSMOOR_TRACKER_CONTROL_START,
} GrassmoorTrackerControlMode;

static const gchar *control_mode_hard_reset[] = {
	"tracker", "reset", "--hard", NULL
};
static const gchar *control_mode_start[] = {
	"tracker", "daemon", "--start", NULL
};

static const gchar * const *grassmoor_tracker_control_modes[] = {
	control_mode_hard_reset,
	control_mode_start,
};

G_STATIC_ASSERT (G_N_ELEMENTS (grassmoor_tracker_control_modes) ==
                 GRASSMOOR_TRACKER_CONTROL_START + 1);

/*
 * run_tracker_control:
 * @mode: mode to run tracker in
 * @error: return location for a #GError, or %NULL
 *
 * Run the tracker utility in a given defined mode and block until it
 * exits. If the utility cannot be run, crashes, or returns a non-zero exit
 * status, return an error.
 *
 * tracker is searched for in the current PATH.
 */
static void
run_tracker_control (GrassmoorTrackerControlMode   mode,
                     GError                      **error)
{
	GSubprocess *subprocess = NULL;

	subprocess = g_subprocess_newv (grassmoor_tracker_control_modes[mode],
	                                G_SUBPROCESS_FLAGS_NONE, error);
	if (subprocess == NULL) {
		return;
	}

	/* Block on it finishing. */
	g_subprocess_wait_check (subprocess, NULL, error);

	g_clear_object (&subprocess);
}

/**
 * grassmoor_tracker_force_reindexing:
 * @error: #GError pointer for getting error messages
 * Returns: %TRUE if the operation succeeded, else %FALSE.
 *
 *  Deletes existing database and forces re-indexing
 **/
gboolean grassmoor_tracker_force_reindexing(GError* error)
{
	GError *child_error = NULL;

	/* FIXME: Ignore the @error propagation for now because the formal
	 * parameter is broken. */
	run_tracker_control (GRASSMOOR_TRACKER_CONTROL_HARD_RESET,
	                     &child_error);
	if (child_error != NULL) {
		g_error_free (child_error);
		return FALSE;
	}

	run_tracker_control (GRASSMOOR_TRACKER_CONTROL_START, &child_error);
	if (child_error != NULL) {
		g_error_free (child_error);
		return FALSE;
	}

	return TRUE;
}

/******************************************************************************
 * tracker_strip_find_next_block:
 *
 *****************************************************************************/
static gboolean tracker_strip_find_next_block (const gchar    *original,
		const gunichar  open_char,
		const gunichar  close_char,
		gint           *open_pos,
		gint           *close_pos)
{
	const gchar *p1, *p2;

	if (open_pos)
	{
		*open_pos = -1;
	}

	if (close_pos)
	{
		*close_pos = -1;
	}

	p1 = g_utf8_strchr (original, -1, open_char);
	if (p1)
	{
		if (open_pos)
		{
			*open_pos = p1 - original;
		}

		p2 = g_utf8_strchr (g_utf8_next_char (p1), -1, close_char);
		if (p2)
		{
			if (close_pos)
			{
				*close_pos = p2 - original;
			}

			return TRUE;
		}
	}

	return FALSE;
}

/******************************************************************************
 * tracker_search_invalid_blocks
 *
 *****************************************************************************/
static void tracker_search_invalid_blocks(GString *pStrNoBlocks,const gchar *pTemp)
{
	const gunichar uniBlocks[5][2] = {
				{ '(', ')' },
				{ '{', '}' },
				{ '[', ']' },
				{ '<', '>' },
				{  0,   0  }
	};
	gboolean bBlocksDone = FALSE;
	while (!bBlocksDone)
	{
		gint inPosition1;
		gint inPosition2;
		gint inIndexLoop;

		inPosition2 = -1;
		inPosition1 = -1;

		for (inIndexLoop = 0; uniBlocks[inIndexLoop][0] != 0; inIndexLoop++)
		{
			gint inStart;
			gint inEnd;

			// Go through all the  blocks and  find the earliest block.
			if (tracker_strip_find_next_block (pTemp, uniBlocks[inIndexLoop][0], uniBlocks[inIndexLoop][1], &inStart, &inEnd))
			{
				if (-1 == inPosition1 || inStart < inPosition1)
				{
					inPosition2 = inEnd;
					inPosition1 = inStart;
				}
			}
		}
		// If either are -1 then  we didn't find any. 
		if (-1 == inPosition1)
		{
			//No Blocks Found.
			g_string_append (pStrNoBlocks, pTemp);
			bBlocksDone = TRUE;
		}
		else
		{
			if (inPosition1 > 0)
			{
				g_string_append_len (pStrNoBlocks, pTemp, inPosition1);
			}

			pTemp = g_utf8_next_char (inPosition2 + pTemp);

			// Repeat the same operation for position AFTER block 
			if ('\0' == *pTemp)
			{
				bBlocksDone = TRUE;
			}
		}
	}
}

/******************************************************************************
 * tracker_strip_invalid_entities:
 *
 *****************************************************************************/
static gchar *tracker_strip_invalid_entities (const gchar *pOriginal)
{
	GString *pStrNoBlocks;
	gchar *pString;
	gchar **pStrv;
	const gchar *pTemp;
	const gchar *pConvertCharsDelimiter = " ";
	const gchar *pConvertChars = "\t";
	const gchar *pInvalidCharsDelimiter = "*";
	const gchar *pInvalidChars = "()[]<>{}_!@#$^&*+=|\\/\"'?~";

	pStrNoBlocks = g_string_new("");
	pTemp = pOriginal;

	tracker_search_invalid_blocks(pStrNoBlocks,pTemp);
	
	pString = g_string_free (pStrNoBlocks, FALSE);

	// Strip the  invalid characters.
	g_strdelimit (pString,pInvalidChars,*pInvalidCharsDelimiter);
	pStrv = g_strsplit (pString,pInvalidCharsDelimiter,-1);
	// Free the string
	g_free (pString);
	pString = g_strjoinv (NULL,pStrv);
	g_strfreev (pStrv);

	// Convert the characters.
	g_strdelimit (pString, pConvertChars, *pConvertCharsDelimiter);
	pStrv = g_strsplit (pString, pConvertCharsDelimiter, -1);
	// Free the string
	g_free (pString);
	pString = g_strjoinv (pConvertCharsDelimiter, pStrv);
	g_strfreev (pStrv);

	// Remove all the double space.
	pStrv = g_strsplit (pString, "  ", -1);
	// Free the string
	g_free (pString);
	pString = g_strjoinv (" ", pStrv);
	g_strfreev (pStrv);

	// Strip the trailing and leading white space.
	g_strstrip (pString);

	return pString;
}

/*********************************************************************
 * tracker_generate_md5:
 *
 *********************************************************************/
static gchar* tracker_generate_md5(const gchar* sAlbumOrArtist)
{
	gchar *TempBufferStep1=NULL, *TempBufferStep2=NULL, *TempBufferStep3=NULL;
	if((sAlbumOrArtist) && (*sAlbumOrArtist))
	{
		TempBufferStep1 = tracker_strip_invalid_entities(sAlbumOrArtist);
		TempBufferStep2 = g_ascii_strdown (TempBufferStep1, PATH_MAX);
		g_free(TempBufferStep1);
		TempBufferStep3 = g_compute_checksum_for_string (G_CHECKSUM_MD5, TempBufferStep2, -1);
		g_free(TempBufferStep2);
	}
	else
	{
		/* if no artist or album then take MD5 of white space */
		TempBufferStep2 = " ";
		TempBufferStep3 = g_compute_checksum_for_string (G_CHECKSUM_MD5, TempBufferStep2, -1);
	}
	return TempBufferStep3;
}

/*********************************************************************
 * tracker_check_in_thumb_dir:
 *
 *********************************************************************/
static gchar *tracker_check_in_thumb_dir(const gchar *pUri)
{
	gchar *ThumbsDir = NULL;
	gchar* pThumbPath = NULL;

	DEBUG ("get thumb for uri = %s",pUri);
	/*Compute the md5checksum name */
	gchar *md5Title = g_compute_checksum_for_string (G_CHECKSUM_MD5, pUri, -1);
	if(md5Title != NULL)
	{
		ThumbsDir = g_build_filename (g_get_home_dir (), ".cache",
                                            "thumbnails", "normal", NULL);
		/*Return the full uri of the thumbnail*/
		pThumbPath = g_strconcat (ThumbsDir, "/", md5Title, ".png", NULL);
		g_free (ThumbsDir);
		g_free (md5Title);
	}
	DEBUG (" Thumb from thumbnail dir using tracker: %s", pThumbPath);

	return pThumbPath;
} 

/**************** EXPOSED APIs
 * **********************************************************/

/**
 * grassmoor_tracker_get_albumart_url:
 * @sAlbum: Album name
 * @sArtist: Artist Name
 *
 * Generates and gives album art URL for album artist pair
 *
 * Returns: (transfer full): a string for Album art URL
 **/
gchar *
grassmoor_tracker_get_albumart_url (const gchar *pUri,
                                    const gchar* sAlbum,
                                    const gchar* sArtist)
{
	GString* sAlbumartUrl;
	gchar *TempBufferStep3=NULL;
	gchar *pThumb = NULL;
	/* As Per Media Art specification @ http://live.gnome.org/MediaArtStorageSpec */
	sAlbumartUrl = g_string_new("file:///");
	sAlbumartUrl = g_string_append(sAlbumartUrl, g_get_home_dir());
	sAlbumartUrl = g_string_append(sAlbumartUrl, "/.cache/media-art/album-");

	TempBufferStep3 = tracker_generate_md5(sArtist);
	sAlbumartUrl = g_string_append(sAlbumartUrl, TempBufferStep3);
	sAlbumartUrl = g_string_append(sAlbumartUrl, "-");
	g_free(TempBufferStep3);

	TempBufferStep3 = tracker_generate_md5(sAlbum);
	sAlbumartUrl = g_string_append(sAlbumartUrl, TempBufferStep3);
	sAlbumartUrl = g_string_append(sAlbumartUrl, ".jpeg");
	g_free(TempBufferStep3);

	/* check if file exists , otherwist try to generate using taglib */
	if(tracker_check_thumb_exists(sAlbumartUrl->str) == FALSE)	
	{
		g_string_free(sAlbumartUrl, TRUE);
		/* if albumart is not present in the media-art path, check for thumbnail path because if
 		 * mediaart is not embedded in the file, thumb can be available in thumbnails  path */
		pThumb = tracker_check_in_thumb_dir(pUri);
		if(pThumb == NULL || (tracker_check_thumb_exists (pThumb) == FALSE ) )
		{
			g_free(pThumb);
			pThumb = NULL;
			pThumb = tracker_taglib_get_thumbnail(pUri);
		}
		sAlbumartUrl = g_string_new(pThumb);
		g_free(pThumb);
	}
	return g_string_free (sAlbumartUrl, FALSE);

}

/**
 * grassmoor_tracker_generate_thumbnail_uri:
 * @sFileUrl: media uri for thumb generation
 *
 * Generates and gives album art URL for album artist pair
 *
 * Returns: (transfer full): a string as thumbnail uri
 **/
gchar *
grassmoor_tracker_generate_thumbnail_uri (const gchar* sFileUrl)
{
	gchar* thumb_uri = NULL;
	gchar* md5ofUri = NULL;
	GString *sThumbnail = NULL;

	if(sFileUrl)
	{
		/* As per Thumbnail Specification @ http://live.gnome.org/ThumbnailerSpec */
		/* http://live.gnome.org/Tracker/Documentation/ThumbnailsHandling         */

		md5ofUri = g_compute_checksum_for_string (G_CHECKSUM_MD5, sFileUrl, -1);
		//g_print("%s %s \n", md5ofUri, sFileUrl);
		if(md5ofUri)
		{
			thumb_uri = g_strdup_printf("file://%s/.cache/thumbnails/normal/%s.png", g_get_home_dir(), md5ofUri);
			g_free(md5ofUri);
			if(tracker_check_thumb_exists(thumb_uri) == FALSE)
			{
				 if(thumb_uri)
  		                      g_free(thumb_uri);
				
		         thumb_uri = tracker_taglib_get_thumbnail(sFileUrl);
			}
			if(thumb_uri)
			{
				DEBUG ("Thumb uri = %s", thumb_uri);
				sThumbnail = g_string_new(thumb_uri);	
				g_free(thumb_uri);
			}
		}
	}
	return g_string_free (sThumbnail, FALSE);
}
/*******************************************************************************
 * grassmoor_tracker_get_media_type:
 * @sFileUrl: URL for the file whose type is required
 * @error: #GError pointer for getting error messages
 * Returns: #GrassmoorMediaType for input URL file
 *
 *  TODO: Yet to be implemented
 *  Decodes file type from URL and gives in the form of GrassmoorMediaType
 ***************************************************************************/
GrassmoorMediaType grassmoor_tracker_get_media_type(const gchar *sFileUrl, GError *error)
{
	return GRASSMOOR_TRACKER_MEDIA_TYPE_UNKNOWN;
}

#if 0
/******************************************************************************
 * tracker_strip_invalid_entities:
 *
 *****************************************************************************/
static gchar *tracker_strip_invalid_entities (const gchar *pOriginal)
{
	GString *pStrNoBlocks;
	gchar **pStrv;
	gchar *pString;
	gboolean bBlocksDone = FALSE;
	const gchar *pTemp;
	const gchar *pInvalidChars = "()[]<>{}_!@#$^&*+=|\\/\"'?~";
	const gchar *pInvalidCharsDelimiter = "*";
	const gchar *pConvertChars = "\t";
	const gchar *pConvertCharsDelimiter = " ";
	const gunichar uniBlocks[5][2] = {
			{ '(', ')' },
			{ '{', '}' },
			{ '[', ']' },
			{ '<', '>' },
			{  0,   0  }
	};

	pStrNoBlocks = g_string_new ("");

	pTemp = pOriginal;

	while (!bBlocksDone)
	{
		gint pos1, pos2, i;

		pos1 = -1;
		pos2 = -1;

		for (i = 0; uniBlocks[i][0] != 0; i++)
		{
			gint start, end;

			/* Go through blocks, find the earliest block we can */
			if (tracker_strip_find_next_block (pTemp, uniBlocks[i][0], uniBlocks[i][1], &start, &end))
			{
				if (pos1 == -1 || start < pos1)
				{
					pos1 = start;
					pos2 = end;
				}
			}
		}

		/* If either are -1 we didn't find any */
		if (pos1 == -1)
		{
			/* This means no blocks were found */
			g_string_append (pStrNoBlocks, pTemp);
			bBlocksDone = TRUE;
		}
		else
		{
			/* Append the test BEFORE the block */
			if (pos1 > 0)
			{
				g_string_append_len (pStrNoBlocks, pTemp, pos1);
			}

			pTemp = g_utf8_next_char (pTemp + pos2);

			/* Do same again for position AFTER block */
			if (*pTemp == '\0')
			{
				bBlocksDone = TRUE;
			}
		}
	}

	pString = g_string_free (pStrNoBlocks, FALSE);

	/* Now strip invalid chars */
	g_strdelimit (pString, pInvalidChars, *pInvalidCharsDelimiter);
	pStrv = g_strsplit (pString, pInvalidCharsDelimiter, -1);
	g_free (pString);
	pString = g_strjoinv (NULL, pStrv);
	g_strfreev (pStrv);

	/* Now convert chars */
	g_strdelimit (pString, pConvertChars, *pConvertCharsDelimiter);
	pStrv = g_strsplit (pString, pConvertCharsDelimiter, -1);
	g_free (pString);
	pString = g_strjoinv (pConvertCharsDelimiter, pStrv);
	g_strfreev (pStrv);

	/* Now remove double spaces */
	pStrv = g_strsplit (pString, "  ", -1);
	g_free (pString);
	pString = g_strjoinv (" ", pStrv);
	g_strfreev (pStrv);

	/* Now strip leading/trailing white space */
	g_strstrip (pString);

	return pString;
}
#endif

static void
grassmoor_tracker_dispose (GObject *object)
{
  GrassmoorTracker *self = GRASSMOOR_TRACKER (object);

  g_clear_object (&self->miner_mgr);
  g_clear_object (&self->conn);

  G_OBJECT_CLASS (grassmoor_tracker_parent_class)->dispose (object);
}

static void
grassmoor_tracker_class_init (GrassmoorTrackerClass *klass)
{
  GObjectClass *gclass = G_OBJECT_CLASS (klass);

  gclass->dispose = grassmoor_tracker_dispose;
}

static void
grassmoor_tracker_init (GrassmoorTracker *self)
{
  GError *error = NULL;

  DEBUG ("entered");

  self->miner_mgr = tracker_miner_manager_new ();
  run_tracker_control (GRASSMOOR_TRACKER_CONTROL_START, NULL);

  self->conn = tracker_sparql_connection_get (NULL, &error);
  /* FIXME: GObject creation can't fail. Is this ok or should we have _prepare()
   * method? */
  g_assert_no_error (error);

  DEBUG ("Exit");
}

GrassmoorTracker *
grassmoor_tracker_new (void)
{
  return g_object_new (GRASSMOOR_TYPE_TRACKER, NULL);
}

/* GrassmoorMediaInfo API */

static GrassmoorMediaInfo *
grassmoor_media_info_copy (GrassmoorMediaInfo *info)
{
  GrassmoorMediaInfo *copy = g_new0 (GrassmoorMediaInfo, 1);

  copy->enSource = info->enSource;
  copy->enFileType = info->enFileType;
  copy->url = g_strdup (info->url);
  copy->title = g_strdup (info->title);
  copy->content_type = g_strdup (info->content_type);
  copy->iSizeInBytes = info->iSizeInBytes;

  if (TRACKER_IS_AUDIO_VIDEO (info->enFileType))
    {
      copy->Info.AudioVideoFileInfo.iTrackNum = info->Info.AudioVideoFileInfo.iTrackNum;
      copy->Info.AudioVideoFileInfo.iDuration = info->Info.AudioVideoFileInfo.iDuration;
      copy->Info.AudioVideoFileInfo.artist = g_strdup (info->Info.AudioVideoFileInfo.artist);
      copy->Info.AudioVideoFileInfo.album = g_strdup (info->Info.AudioVideoFileInfo.album);
      copy->Info.AudioVideoFileInfo.genre = g_strdup (info->Info.AudioVideoFileInfo.genre);
      copy->Info.AudioVideoFileInfo.year = g_strdup (info->Info.AudioVideoFileInfo.year);
      copy->Info.AudioVideoFileInfo.album_art_url = g_strdup (info->Info.AudioVideoFileInfo.album_art_url);
    }
  else if (TRACKER_IS_PICTURE (info->enFileType))
    {
      copy->Info.ImageFileInfo.iHeight = info->Info.ImageFileInfo.iHeight;
      copy->Info.ImageFileInfo.iWidth = info->Info.ImageFileInfo.iWidth;
      copy->Info.ImageFileInfo.date = g_strdup (info->Info.ImageFileInfo.date);
      copy->Info.ImageFileInfo.tag = g_strdup (info->Info.ImageFileInfo.tag);
    }
  else if (info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_PLAYLIST)
    {
      copy->Info.PlaylistFileInfo.iNumOfElements = info->Info.PlaylistFileInfo.iNumOfElements;
    }
  else if (info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_FOLDER)
    {
      copy->Info.FolderInfo.iNumOfElements = info->Info.FolderInfo.iNumOfElements;
    }

  return copy;
}

G_DEFINE_BOXED_TYPE (GrassmoorMediaInfo, grassmoor_media_info, grassmoor_media_info_copy,
                     grassmoor_media_info_free)

/* FIXME: the exact semantic of some of those fields isn't that clear.
 * Please clarify and add examples of possible values */

/**
 * grassmoor_media_info_get_source:
 * @info: a #GrassmoorMediaInfo
 *
 * Return the type of volume on which the media described by @info
 * is stored.
 *
 * Returns: the #GrassmoorVolumeType of the media
 */
GrassmoorVolumeType
grassmoor_media_info_get_source (GrassmoorMediaInfo *info)
{
  return info->enSource;
}

/**
 * grassmoor_media_info_get_media_type:
 * @info: a #GrassmoorMediaInfo
 *
 * Return the type of the media described by @info.
 *
 * Returns: the #GrassmoorMediaType of the media
 */
GrassmoorMediaType
grassmoor_media_info_get_media_type (GrassmoorMediaInfo *info)
{
  return info->enFileType;
}

/**
 * grassmoor_media_info_is_audio_video:
 * @info: a #GrassmoorMediaInfo
 *
 * Check if the media described by @info is an audio or video one.
 *
 * Returns: %TRUE, if the type of @info is either
 * %GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO or %GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO
 */
gboolean
grassmoor_media_info_is_audio_video (GrassmoorMediaInfo *info)
{
  return TRACKER_IS_AUDIO_VIDEO (info->enFileType);
}

/**
 * grassmoor_media_info_get_url:
 * @info: a #GrassmoorMediaInfo
 *
 * Return the URL of the media described by @info.
 *
 * Returns: the URL of the media
 */
const gchar *
grassmoor_media_info_get_url (GrassmoorMediaInfo *info)
{
  return info->url;
}

/**
 * grassmoor_media_info_get_title:
 * @info: a #GrassmoorMediaInfo
 *
 * Return the title of the media described by @info.
 *
 * Returns: the title of the media
 */
const gchar *
grassmoor_media_info_get_title (GrassmoorMediaInfo *info)
{
  return info->title;
}

/**
 * grassmoor_media_info_get_content_type:
 * @info: a #GrassmoorMediaInfo
 *
 * Return the content-type of the media described by @info.
 *
 * Returns: the content-type of the media
 */
const gchar *
grassmoor_media_info_get_content_type (GrassmoorMediaInfo *info)
{
  return info->content_type;
}

/**
 * grassmoor_media_info_get_size:
 * @info: a #GrassmoorMediaInfo
 *
 * Return the size of the media described by @info.
 *
 * Returns: the size of the media
 */
guint
grassmoor_media_info_get_size (GrassmoorMediaInfo *info)
{
  return info->iSizeInBytes;
}

/* audio video */

/**
 * grassmoor_media_info_get_track_number:
 * @info: a #GrassmoorMediaInfo describing an audio or video media
 *
 * Return the track number of the media described by @info.
 *
 * Returns: the track number of the media
 */
guint
grassmoor_media_info_get_track_number (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (TRACKER_IS_AUDIO_VIDEO (info->enFileType), 0);

  return info->Info.AudioVideoFileInfo.iTrackNum;
}

/**
 * grassmoor_media_info_get_duration:
 * @info: a #GrassmoorMediaInfo describing an audio or video media
 *
 * Return the duration of the media described by @info.
 *
 * Returns: the duration of the media
 */
guint
grassmoor_media_info_get_duration (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (TRACKER_IS_AUDIO_VIDEO (info->enFileType), 0);

  return info->Info.AudioVideoFileInfo.iDuration;
}

/**
 * grassmoor_media_info_get_artist:
 * @info: a #GrassmoorMediaInfo describing an audio or video media
 *
 * Return the artist of the media described by @info.
 *
 * Returns: the artist of the media
 */
const gchar *
grassmoor_media_info_get_artist (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (TRACKER_IS_AUDIO_VIDEO (info->enFileType), NULL);

  return info->Info.AudioVideoFileInfo.artist;
}

/**
 * grassmoor_media_info_get_album:
 * @info: a #GrassmoorMediaInfo describing an audio or video media
 *
 * Return the album of the media described by @info.
 *
 * Returns: the album of the media
 */
const gchar *
grassmoor_media_info_get_album (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (TRACKER_IS_AUDIO_VIDEO (info->enFileType), NULL);

  return info->Info.AudioVideoFileInfo.album;
}

/**
 * grassmoor_media_info_get_genre:
 * @info: a #GrassmoorMediaInfo describing an audio or video media
 *
 * Return the genre of the media described by @info.
 *
 * Returns: the genre of the media
 */
const gchar *
grassmoor_media_info_get_genre (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (TRACKER_IS_AUDIO_VIDEO (info->enFileType), NULL);

  return info->Info.AudioVideoFileInfo.genre;
}

/**
 * grassmoor_media_info_get_year:
 * @info: a #GrassmoorMediaInfo describing an audio or video media
 *
 * Return the year of the media described by @info.
 *
 * Returns: the year of the media
 */
const gchar *
grassmoor_media_info_get_year (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (TRACKER_IS_AUDIO_VIDEO (info->enFileType), NULL);

  return info->Info.AudioVideoFileInfo.year;
}

/**
 * grassmoor_media_info_get_album_art_url:
 * @info: a #GrassmoorMediaInfo describing an audio or video media
 *
 * Return an URL of the album's art of the media described by @info.
 *
 * Returns: an URL pointing to an image
 */
const gchar *
grassmoor_media_info_get_album_art_url (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (TRACKER_IS_AUDIO_VIDEO (info->enFileType), NULL);

  return info->Info.AudioVideoFileInfo.album_art_url;
}

/* image */

/**
 * grassmoor_media_info_get_height:
 * @info: a #GrassmoorMediaInfo of type %GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO
 *
 * Return the height of the media described by @info.
 *
 * Returns: the height of the media
 */
guint
grassmoor_media_info_get_height (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO, 0);

  return info->Info.ImageFileInfo.iHeight;
}

/**
 * grassmoor_media_info_get_width:
 * @info: a #GrassmoorMediaInfo of type %GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO
 *
 * Return the width of the media described by @info.
 *
 * Returns: the width of the media
 */
guint
grassmoor_media_info_get_width (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO, 0);

  return info->Info.ImageFileInfo.iWidth;
}

/**
 * grassmoor_media_info_get_date:
 * @info: a #GrassmoorMediaInfo of type %GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO
 *
 * Return the date of the media described by @info.
 *
 * Returns: the date of the media
 */
const gchar *
grassmoor_media_info_get_date (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO, NULL);

  return info->Info.ImageFileInfo.date;
}

/**
 * grassmoor_media_info_get_tag:
 * @info: a #GrassmoorMediaInfo of type %GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO
 *
 * Return the tag of the media described by @info.
 *
 * Returns: the tag of the media
 */
const gchar *
grassmoor_media_info_get_tag (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO, NULL);

  return info->Info.ImageFileInfo.tag;
}

/* playlist and folder */

/**
 * grassmoor_media_info_get_number_of_elements:
 * @info: a #GrassmoorMediaInfo of type %GRASSMOOR_TRACKER_MEDIA_TYPE_FOLDER or %GRASSMOOR_TRACKER_MEDIA_TYPE_PLAYLIST
 *
 * Return the number of elements contained in the folder or playlist described by @info.
 *
 * Returns: the number of elements contained in the media
 */
guint
grassmoor_media_info_get_number_of_elements (GrassmoorMediaInfo *info)
{
  g_return_val_if_fail (info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_FOLDER ||
                        info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_PLAYLIST, 0);

  if (info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_PLAYLIST)
    return info->Info.PlaylistFileInfo.iNumOfElements;
  else if (info->enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_FOLDER)
    return info->Info.FolderInfo.iNumOfElements;
  else
    g_return_val_if_reached (0);
}

/**
 * grassmoor_media_info_free:
 * @info: (transfer full): a #GrassmoorMediaInfo
 *
 * Frees a #GrassmoorMediaInfo
 */
void
grassmoor_media_info_free (GrassmoorMediaInfo *info)
{
  g_return_if_fail (info);

  g_free (info->url);
  g_free (info->content_type);
  g_free (info->title);

  if (TRACKER_IS_AUDIO_VIDEO (info->enFileType))
    {
      g_free (info->Info.AudioVideoFileInfo.album);
      g_free (info->Info.AudioVideoFileInfo.artist);
      g_free (info->Info.AudioVideoFileInfo.genre);
      g_free (info->Info.AudioVideoFileInfo.year);
      g_free (info->Info.AudioVideoFileInfo.album_art_url);
    }
  else if (TRACKER_IS_PICTURE (info->enFileType))
  {
    g_free (info->Info.ImageFileInfo.tag);
    g_free (info->Info.ImageFileInfo.date);
  }

  g_free (info);
}
