/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* libgrassmoor.h */

/* Main header file including subfolders header */

#ifndef __LIBGRASSMOOR_H__
#define __LIBGRASSMOOR_H__

#include <libgrassmoor-av-player.h>
#include <libgrassmoor-audio-recorder.h>
#include <libgrassmoor-camera-capture.h>
#include <libgrassmoor-tracker.h>


#endif /* __LIBGRASSMOOR_H__  */


