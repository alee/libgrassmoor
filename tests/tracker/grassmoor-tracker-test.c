/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libgrassmoor-tracker.h>

void print_from_array(GPtrArray *pResultPointer)
{
  gint iCount = 0;
  g_print("\n");
  if(pResultPointer)
  {
          for(iCount = 0; iCount < pResultPointer->len; iCount++)
          {
             const gchar *line = g_ptr_array_index (pResultPointer, iCount);
             g_print("%s\n", line);
          }
          g_ptr_array_free(pResultPointer, TRUE);
  }
  g_print ("\nTotal elements: %d\n", iCount);
}
void test_grassmoor_tracker_get_url_list_Local_audio (GrassmoorTracker *tracker)
{
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	GPtrArray *pResultPointer = NULL;
	pResultPointer = grassmoor_tracker_get_url_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO,
							GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST, GRASSMOOR_TRACKER_ORDER_TYPE_DESC, 0, 1000, NULL);
	print_from_array(pResultPointer);
}
void test_grassmoor_tracker_get_url_list_Local_video (GrassmoorTracker *tracker)
{
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	GPtrArray *pResultPointer = NULL;
	pResultPointer = grassmoor_tracker_get_url_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO,
													GRASSMOOR_TRACKER_MEDIA_INFO_URL, GRASSMOOR_TRACKER_ORDER_TYPE_DESC, 0, 1000, NULL);
	print_from_array(pResultPointer);
}
void test_grassmoor_tracker_get_url_list_Local_photo (GrassmoorTracker *tracker)
{
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	GPtrArray *pResultPointer = NULL;
	pResultPointer = grassmoor_tracker_get_url_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO,
													GRASSMOOR_TRACKER_MEDIA_INFO_URL, GRASSMOOR_TRACKER_ORDER_TYPE_DESC, 0, 1000, NULL);

	/* Thumbnail Creation Request */
  gchar  **strvUri, **strvMime;
  strvUri = g_new0 (gchar*, pResultPointer->len + 1);
  strvMime = g_new0 (gchar*, pResultPointer->len + 1);
  gint iCount = 0;
  if(pResultPointer)
  {
          for(iCount = 0; iCount < pResultPointer->len; iCount++)
          {
             const char *line = g_ptr_array_index (pResultPointer, iCount);
             strvUri[iCount] = g_strdup (line);
             strvMime[iCount] = g_strdup("image/jpeg");
             gchar *uri = grassmoor_tracker_generate_thumbnail_uri (strvUri[iCount]);
             if (uri)
               {
                 g_print("Thumb URI = %s\n", uri);
                 g_free (uri);
               }
          }
  }
  strvUri[iCount] = NULL;
  strvMime[iCount] = NULL;
 /*************************************/

 print_from_array(pResultPointer);
}
void test_grassmoor_tracker_get_url_list_Local_playlist (GrassmoorTracker *tracker)
{
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	GPtrArray *pResultPointer = NULL;
	pResultPointer = grassmoor_tracker_get_url_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_PLAYLIST,
													GRASSMOOR_TRACKER_MEDIA_INFO_URL, GRASSMOOR_TRACKER_ORDER_TYPE_DESC, 0, 1000, NULL);
	print_from_array(pResultPointer);
}
void test_grassmoor_tracker_get_url_list_Local_documents (GrassmoorTracker *tracker)
{
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	GPtrArray *pResultPointer = NULL;
	pResultPointer = grassmoor_tracker_get_url_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_DOC,
													GRASSMOOR_TRACKER_MEDIA_INFO_URL, GRASSMOOR_TRACKER_ORDER_TYPE_DESC, 0, 1000, NULL);
	print_from_array(pResultPointer);
}
void test_grassmoor_tracker_get_url_list_Local_all_files (GrassmoorTracker *tracker)
{
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	GPtrArray *pResultPointer = NULL;
	pResultPointer = grassmoor_tracker_get_url_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_FILE,
													GRASSMOOR_TRACKER_MEDIA_INFO_URL, GRASSMOOR_TRACKER_ORDER_TYPE_DESC, 0, 1000, NULL);
	print_from_array(pResultPointer);
}
void test_grassmoor_tracker_get_url_list_Local_all_folders (GrassmoorTracker *tracker)
{
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	GPtrArray *pResultPointer = NULL;
	pResultPointer = grassmoor_tracker_get_url_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_FOLDER,
													GRASSMOOR_TRACKER_MEDIA_INFO_URL, GRASSMOOR_TRACKER_ORDER_TYPE_DESC, 0, 1000, NULL);
	print_from_array(pResultPointer);
}
void test_grassmoor_tracker_get_url_list_Local_all_element (GrassmoorTracker *tracker)
{
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	GPtrArray *pResultPointer = NULL;
	pResultPointer = grassmoor_tracker_get_url_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_ALL,
													GRASSMOOR_TRACKER_MEDIA_INFO_URL, GRASSMOOR_TRACKER_ORDER_TYPE_DESC, 0, 1000, NULL);
	print_from_array(pResultPointer);
}
void test_grassmoor_tracker_get_url_list (GrassmoorTracker *tracker)
{
	getchar();
	int ToContinue = 1;
	do
	{
		int a = system("clear");
		a++;
		g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	   g_print ("\n\nList of Test cases: \n");
	   g_print ("1: test_grassmoor_tracker_get_url_list_Local_audio \n");
	   g_print ("2: test_grassmoor_tracker_get_url_list_Local_video \n");
	   g_print ("3: test_grassmoor_tracker_get_url_list_Local_photo \n");
	   g_print ("4: test_grassmoor_tracker_get_url_list_Local_playlist \n");
	   g_print ("5: test_grassmoor_tracker_get_url_list_Local_all_files \n");
	   g_print ("6: test_grassmoor_tracker_get_url_list_Local_all_folders \n");
	   g_print ("7: test_grassmoor_tracker_get_url_list_Local_all_element \n");
	   g_print ("8: test_grassmoor_tracker_get_url_list_Local_documents \n");
	   g_print ("9: Back to previous menu \n");
	   g_print ("Select API to test: ");

	   switch(getchar())
	   {
	   case '1':
		   test_grassmoor_tracker_get_url_list_Local_audio (tracker);
		   getchar();
		   g_print ("\nPress ENTER to continue...\n");
		   getchar();
		   break;
	   case '2':
		   test_grassmoor_tracker_get_url_list_Local_video (tracker);
		   getchar();
		   g_print ("\nPress ENTER to continue...\n");
		   getchar();
		   break;
	   case '3':
		   test_grassmoor_tracker_get_url_list_Local_photo (tracker);
		   getchar();
		   g_print ("\nPress ENTER to continue...\n");
		   getchar();
		   break;
	   case '4':
		   test_grassmoor_tracker_get_url_list_Local_playlist (tracker);
		   getchar();
		   g_print ("\nPress ENTER to continue...\n");
		   getchar();
		   break;
	   case '5':
		   test_grassmoor_tracker_get_url_list_Local_all_files (tracker);
		   getchar();
		   g_print ("\nPress ENTER to continue...\n");
		   getchar();
		   break;
	   case '6':
		   test_grassmoor_tracker_get_url_list_Local_all_folders (tracker);
		   getchar();
		   g_print ("\nPress ENTER to continue...\n");
		   getchar();
		   break;
	   case '7':
		   test_grassmoor_tracker_get_url_list_Local_all_element (tracker);
		   getchar();
		   g_print ("\nPress ENTER to continue...\n");
		   getchar();
		   break;
	   case '8':
		   test_grassmoor_tracker_get_url_list_Local_documents (tracker);
		   getchar();
		   g_print ("\nPress ENTER to continue...\n");
		   getchar();
		   break;
	   case '9':
		   ToContinue = 0;
		   return;
	   default:
		   g_print ("Invalid selection. Try again...\n");
		   break;
	   }
	}
	while(ToContinue);
}
void test_grassmoor_tracker_get_media_info_list (GrassmoorTracker *tracker,
                                                 GrassmoorMediaType enFileType)
{
	getchar();
	int a = system("clear");
	a++;
	guint iCount=0;
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	GPtrArray *pResultPointer = NULL;
	pResultPointer = grassmoor_tracker_get_media_info_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, enFileType, 6,1,0,1000,0);
	if(pResultPointer)
	{
		for(iCount = 0; iCount < pResultPointer->len; iCount++)
		{
                    GrassmoorMediaInfo* MedStruct = g_ptr_array_index(pResultPointer, iCount);
                    g_print("\nUrl: %s\n", grassmoor_media_info_get_url (MedStruct));
                    g_print("Mime: %s\n", grassmoor_media_info_get_content_type (MedStruct));
                    g_print("Size: %d bytes\n", grassmoor_media_info_get_size (MedStruct));
                    if (grassmoor_media_info_get_source (MedStruct) == GRASSMOOR_TRACKER_VOLUME_REMOVABLE)
                    {
                      g_print("Volume: Removable\n");
                    }
                    else
                      {
                        g_print("Volume: Local\n");
                      }
                    if((enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO) || (enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO))
                    {
                      g_print("Track Num: %d\n", grassmoor_media_info_get_track_number (MedStruct));
                      g_print("Title: %s\n", grassmoor_media_info_get_title (MedStruct));
                      g_print("Album: %s\n", grassmoor_media_info_get_album (MedStruct));
                      g_print("Artist: %s\n", grassmoor_media_info_get_artist (MedStruct));
                      g_print("Genre: %s\n", grassmoor_media_info_get_genre (MedStruct));
                      g_print("Year: %s\n", grassmoor_media_info_get_year (MedStruct));
                      g_print("Duration: %d Sec\n", grassmoor_media_info_get_duration (MedStruct));
                    }
                    else if(enFileType == GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO)
                    {
                      g_print("Title: %s\n", grassmoor_media_info_get_title (MedStruct));
                      g_print("Height: %d\n", grassmoor_media_info_get_width (MedStruct));
                      g_print("Width: %d\n", grassmoor_media_info_get_width (MedStruct));
                      g_print("Tag: %s\n", grassmoor_media_info_get_tag (MedStruct));
                      g_print("Date: %s\n", grassmoor_media_info_get_date (MedStruct));
                    }
		}
		g_ptr_array_free(pResultPointer, TRUE);
	}
	g_print ("\nTotal elements: %d\n", iCount);
}
void test_grassmoor_tracker_get_media_info_list_selection (GrassmoorTracker *tracker)
{
  getchar();
  int ToContinue = 1;
  do
  {
          int a = system("clear");
          a++;
          g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
     g_print ("\n\nList of Test cases: \n");
     g_print ("1: test_grassmoor_tracker_get_media_info_list_Local_audio \n");
     g_print ("2: test_grassmoor_tracker_get_media_info_list_Local_video \n");
     g_print ("3: test_grassmoor_tracker_get_media_info_list_Local_photo \n");
     g_print ("4: test_grassmoor_tracker_get_media_info_list_Local_playlist \n");
     g_print ("5: test_grassmoor_tracker_get_media_info_list_Local_all_files \n");
     g_print ("9: Back to previous menu \n");
     g_print ("Select API to test: ");

     switch(getchar())
     {
     case '1':
       test_grassmoor_tracker_get_media_info_list (tracker, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO);
             g_print ("\nPress ENTER to continue...\n");
             getchar();
             break;
     case '2':
       test_grassmoor_tracker_get_media_info_list (tracker, GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO);
             g_print ("\nPress ENTER to continue...\n");
             getchar();
             break;
     case '3':
       test_grassmoor_tracker_get_media_info_list (tracker, GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO);
             g_print ("\nPress ENTER to continue...\n");
             getchar();
             break;
     case '4':
       test_grassmoor_tracker_get_media_info_list (tracker, GRASSMOOR_TRACKER_MEDIA_TYPE_PLAYLIST);
             g_print ("\nPress ENTER to continue...\n");
             getchar();
             break;
     case '5':
       test_grassmoor_tracker_get_media_info_list (tracker, GRASSMOOR_TRACKER_MEDIA_TYPE_ALL);
             g_print ("\nPress ENTER to continue...\n");
             getchar();
             break;
     case '9':
             ToContinue = 0;
             return;
     default:
             g_print ("Invalid selection. Try again...\n");
             break;
     }
  }
  while(ToContinue);
}
void test_grassmoor_tracker_get_media_file_info (GrassmoorTracker *tracker)
{
	char FilePath[PATH_MAX];
	guint FileType;
	getchar();
	while(1)
        {
	int a = system("clear");
	a++;
	g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
	g_print ("\n Enter File Path: ");
	a = scanf("%s", FilePath);
	g_print ("\n Select type: \n");
	g_print ("0: GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO \n");
        g_print ("1: GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO \n");
        g_print ("2: GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO \n");
        g_print ("3: GRASSMOOR_TRACKER_MEDIA_TYPE_PLAYLIST \n");
        g_print ("4: GRASSMOOR_TRACKER_MEDIA_TYPE_DOC \n");
        g_print ("5: GRASSMOOR_TRACKER_MEDIA_TYPE_FOLDER \n");
        g_print ("8: GRASSMOOR_TRACKER_MEDIA_TYPE_ALL \n");
	a = scanf("%d", &FileType);
	if(FileType < 9)
        {
          GrassmoorMediaInfo* MedStruct = grassmoor_tracker_get_media_file_info (tracker, FilePath, FileType, NULL);
          if(MedStruct)
          {
              g_print("\nUrl: %s\n", grassmoor_media_info_get_url (MedStruct));
              g_print("Mime: %s\n", grassmoor_media_info_get_content_type (MedStruct));
              g_print("Size: %d bytes\n", grassmoor_media_info_get_size (MedStruct));
              if (grassmoor_media_info_get_source (MedStruct) == GRASSMOOR_TRACKER_VOLUME_REMOVABLE)
              {
                g_print("Volume: Removable\n");
              }
              else
                {
                  g_print("Volume: Local\n");
                }
              if (grassmoor_media_info_is_audio_video (MedStruct))
              {
                g_print("Track Num: %d\n", grassmoor_media_info_get_track_number (MedStruct));
                g_print("Title: %s\n", grassmoor_media_info_get_title (MedStruct));
                g_print("Album: %s\n", grassmoor_media_info_get_album (MedStruct));
                g_print("Artist: %s\n", grassmoor_media_info_get_artist (MedStruct));
                g_print("Genre: %s\n", grassmoor_media_info_get_genre (MedStruct));
                g_print("Year: %s\n", grassmoor_media_info_get_year (MedStruct));
                g_print("Duration: %d Sec\n", grassmoor_media_info_get_duration (MedStruct));
              }
              else if (grassmoor_media_info_get_media_type (MedStruct) == GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO)
              {
                g_print("Title: %s\n", grassmoor_media_info_get_title (MedStruct));
                g_print("Height: %d\n", grassmoor_media_info_get_width (MedStruct));
                g_print("Width: %d\n", grassmoor_media_info_get_width (MedStruct));
                g_print("Tag: %s\n", grassmoor_media_info_get_tag (MedStruct));
                g_print("Date: %s\n", grassmoor_media_info_get_date (MedStruct));
              }
              grassmoor_media_info_free (MedStruct);
          }
          getchar();
          g_print ("\nPress ENTER to continue...\n");
          getchar();
          break;
        }
	g_print ("Invalid selection. Try again...\n");
        }
}

void test_grassmoor_tracker_get_meta_list (GrassmoorTracker *tracker)
{
        GPtrArray *pResultPointer = NULL;

        getchar();
        int ToContinue = 1;
        do
        {
                int a = system("clear");
                a++;
                g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
           g_print ("\n\nList of Test cases: \n");
           g_print ("1: test_grassmoor_tracker_get_meta_list_Artists \n");
           g_print ("2: test_grassmoor_tracker_get_meta_list_Albums \n");
           g_print ("3: test_grassmoor_tracker_get_meta_list_Genres \n");
           g_print ("4: test_grassmoor_tracker_get_meta_list_Titles \n");
           g_print ("9: Back to previous menu \n");
           g_print ("Select API to test: ");

           switch(getchar())
           {
           case '1':
             pResultPointer = grassmoor_tracker_get_meta_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST, 0, 1000, NULL);
             print_from_array(pResultPointer);
                   getchar();
                   g_print ("\nPress ENTER to continue...\n");
                   getchar();
                   break;
           case '2':
             pResultPointer = grassmoor_tracker_get_meta_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM, 0, 1000, NULL);
             print_from_array(pResultPointer);
             getchar();
                   g_print ("\nPress ENTER to continue...\n");
                   getchar();
                   break;
           case '3':
             pResultPointer = grassmoor_tracker_get_meta_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_INFO_GENRE, 0, 1000, NULL);
             print_from_array(pResultPointer);
                   getchar();
                   g_print ("\nPress ENTER to continue...\n");
                   getchar();
                   break;
           case '4':
             pResultPointer = grassmoor_tracker_get_meta_list (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_INFO_TITLE, 0, 1000, NULL);
             print_from_array(pResultPointer);
                   getchar();
                   g_print ("\nPress ENTER to continue...\n");
                   getchar();
                   break;
           case '9':
                   ToContinue = 0;
                   return;
           default:
                   g_print ("Invalid selection. Try again...\n");
                   break;
           }
        }
        while(ToContinue);
}

void test_grassmoor_tracker_get_meta_list_for (GrassmoorTracker *tracker)
{
  GPtrArray *pResultPointer = NULL;
   getchar();
   int ToContinue = 1;
   do
   {
       char* input = NULL;
       input = malloc (sizeof (char) * 1024);
       size_t nbytes = 1024-1;
       char* b;
           int a = system("clear");
           a++;
           g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
      g_print ("\n\nList of Test cases: \n");
      g_print ("1: Get List of tracks for any Artist \n");
      g_print ("2: Get List of tracks for Any Album \n");
      g_print ("3: Get List of tracks for any Genre \n");
      g_print ("9: Back to previous menu \n");
      g_print ("Select API to test: ");

      switch(getchar())
      {
      case '1':
        getchar();
        g_print ("\n Enter Artist: ");
        b = (char *) getline(&input, &nbytes, stdin);
        b++;
        char* LineTerm = g_strrstr(input, "\n");
        *LineTerm = (char)0;
        g_print ("\n you entered: %s", input);
        pResultPointer = grassmoor_tracker_get_meta_list_for (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL,
            GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST, input,
            GRASSMOOR_TRACKER_MEDIA_INFO_URL, 0, 1000, NULL);
        print_from_array(pResultPointer);
              g_print ("\nPress ENTER to continue...\n");
              getchar();
              break;
      case '2':
        getchar();
        g_print ("\n Enter Album: ");
        b = (char *) getline(&input, &nbytes, stdin);
        b++;
        LineTerm = g_strrstr(input, "\n");
        *LineTerm = (char)0;
        pResultPointer = grassmoor_tracker_get_meta_list_for (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL,
            GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM, input,
            GRASSMOOR_TRACKER_MEDIA_INFO_URL, 0, 1000, NULL);
        print_from_array(pResultPointer);

              g_print ("\nPress ENTER to continue...\n");
              getchar();
              break;
      case '3':
        getchar();
        g_print ("\n Enter Genre: ");
        b = (char *) getline(&input, &nbytes, stdin);
        b++;
        LineTerm = g_strrstr(input, "\n");
        *LineTerm = (char)0;
        pResultPointer = grassmoor_tracker_get_meta_list_for (tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL,
            GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM, input,
            GRASSMOOR_TRACKER_MEDIA_INFO_URL, 0, 1000, NULL);
        print_from_array(pResultPointer);
              g_print ("\nPress ENTER to continue...\n");
              getchar();
              break;
      case '9':
              ToContinue = 0;
              return;
      default:
              g_print ("Invalid selection. Try again...\n");
              break;
      }
      free(input);
   }
   while(ToContinue);
}

void test_grassmoor_tracker_get_url_list_for_playlist (GrassmoorTracker *tracker)
{
  GPtrArray *pResultPointer = NULL;
   getchar();
   int ToContinue = 1;
   do
   {
       char* input = NULL;
       input = malloc (sizeof (char) * 1024);
       size_t nbytes = 1024-1;
       char* b;
           int a = system("clear");
           a++;
           g_print ("\n\n*** Test: %s ***\n", __FUNCTION__);
      g_print ("\n\nList of Test cases: \n");
      g_print ("1: Get List of tracks for any Plylist \n");
      g_print ("9: Back to previous menu \n");
      g_print ("Select option: ");

      switch(getchar())
      {
      case '1':
        getchar();
        g_print ("\n Enter playlist path: ");
        b = (char *) getline(&input, &nbytes, stdin);
        b++;
        char* LineTerm = g_strrstr(input, "\n");
         *LineTerm = (char)0;
        pResultPointer = grassmoor_tracker_get_url_list_for_playlist (tracker, input, 0, 1000, NULL);
        print_from_array(pResultPointer);
              g_print ("\nPress ENTER to continue...\n");
              getchar();
              break;
      case '9':
              ToContinue = 0;
              return;
      default:
              g_print ("Invalid selection. Try again...\n");
              break;
      }
      free(input);
   }
   while(ToContinue);
}

/**
 * SECTION:main
 * @short_description: Test programm for libmeta-tracker
 *
 * @see_also: #For more information, see <ulink role="online-location"
 *             url="http://moblin.org">http://moblin.org</ulink>
 * @stability: Stable
 *
 * Parses command-line.  Starts sample console app.
 */
int main(int argc, char* argv[])
{
  GrassmoorTracker *tracker;
  gchar *album_art_url;
  int ToContinue = 1;

  tracker = grassmoor_tracker_new ();

	do
	{
            int a = system("clear");
            a++;
	   g_print ("\n\t\t ************* Program: meta-tracker-test ************\n");
	   g_print ("\n\nList of APIs: \n");
	   g_print ("1: grassmoor_tracker_get_url_list \n");
	   g_print ("2: grassmoor_tracker_get_media_file_info \n");
	   g_print ("3: grassmoor_tracker_get_url_list_for_playlist \n");
	   g_print ("4: grassmoor_tracker_get_media_info_list \n");
	   g_print ("5: grassmoor_tracker_get_meta_list \n");
	   g_print ("6: grassmoor_tracker_get_meta_list_for \n");
	   g_print ("7: grassmoor_tracker_get_browse_list_from \n");
	   g_print ("8: grassmoor_tracker_get_search_list \n");
	   g_print ("9: Exit \n");
	   g_print ("Select API to test: ");

	   switch(getchar())
	   {
	   case '1':
		   test_grassmoor_tracker_get_url_list (tracker);
		   break;
	   case '2':
		   test_grassmoor_tracker_get_media_file_info (tracker);
		   break;
	   case '3':
	           test_grassmoor_tracker_get_url_list_for_playlist (tracker);
		   break;
	   case '4':
	           test_grassmoor_tracker_get_media_info_list_selection (tracker);
		   break;
	   case '5':
	           test_grassmoor_tracker_get_meta_list (tracker);
		   break;
	   case '6':
	     test_grassmoor_tracker_get_meta_list_for (tracker);
		   break;
	   case '7':
		   break;
	   case '8':
	     album_art_url = grassmoor_tracker_get_albumart_url("", "", "");
	     g_print ("\nAlbumArt Path: %s\n", album_art_url);
	     g_free (album_art_url);
	     getchar();
		   break;
	   case '9':
		   ToContinue = 0;
		   break;
	   default:
		   g_print ("Invalid selection. Try again...\n");
		   break;
	   }
	}
	while(ToContinue);

  g_object_unref (tracker);
  return 0;
}
