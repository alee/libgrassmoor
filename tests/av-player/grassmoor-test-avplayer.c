/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <libgrassmoor-av-player.h>
#include <gst/gst.h>
#include <stdlib.h>

gint main(gint argc, gchar **argv)
{
	int clInErr = clutter_init(&argc, &argv);
        if (clInErr != CLUTTER_INIT_SUCCESS)
                return -1;

	if (argc != 2)
	{
		g_print (" Usage is %s, <complete-audio-file-path>, ex: file:///media/audio.mp3 \n", argv[0]);
		exit (1);
	}
        
	ClutterActor *pStage, *videoActor;
	
        ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };

        pStage = clutter_stage_new ();
        clutter_actor_set_background_color (pStage, &black);
        clutter_actor_set_size (pStage, 728, 480);

	clutter_gst_init(&argc, &argv);
	GrassmoorAVPlayer *player = grassmoor_av_player_new (GRASSMOOR_AV_PLAYER_MODE_VIDEO);
	//g_signal_connect(player, "video-actor-created", G_CALLBACK(actor_created_cb), pStage);
	videoActor = grassmoor_av_player_get_video_actor (player);
	clutter_gst_playback_set_uri (CLUTTER_GST_PLAYBACK (player), argv[1]);
	clutter_gst_player_set_playing (CLUTTER_GST_PLAYER (player), TRUE);

	clutter_actor_set_size(videoActor, 162, 162);       
	clutter_actor_add_child(pStage, CLUTTER_ACTOR(videoActor));
        clutter_actor_show (CLUTTER_ACTOR(videoActor));
        clutter_actor_show (pStage);

	clutter_main();
	return 0;
}
