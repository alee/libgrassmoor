/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <libgrassmoor-audio-recorder.h>

static int count = 0;
static void start_rec (GrassmoorAudioRecorder *recorder, gpointer userData)
{
}

static void pause_rec (GrassmoorAudioRecorder *recorder, gpointer userData)
{
}

static void resume_rec (GrassmoorAudioRecorder *recorder, gpointer userData)
{
}

static void comp_rec (GrassmoorAudioRecorder *recorder, const gchar *fileLoc, gpointer userData)
{
}

static gboolean pause_recorder (gpointer userData)
{
	if(!GRASSMOOR_IS_AUDIO_RECORDER(userData))
		return FALSE;

	GrassmoorAudioRecorder *recorder = GRASSMOOR_AUDIO_RECORDER(userData);

	if(count == 0)
	{
		g_object_set(recorder, "recording", FALSE, NULL);
		count ++;
		return TRUE;
	}
	else
	{
		g_object_set(recorder, "recording", TRUE, NULL);
		return FALSE;
	}
}

gint main(gint argc, gchar **argv)
{
	gchar *location = NULL;
	gst_init(&argc, &argv);
	
	GrassmoorAudioRecorder *recorder = grassmoor_audio_recorder_new();

	if(GRASSMOOR_IS_AUDIO_RECORDER(recorder))
	{
		location = g_strconcat(g_get_home_dir(), "/", "record.spx", NULL);

		grassmoor_audio_recorder_set_file_location (recorder, location);
		grassmoor_audio_recorder_start_recording (recorder);

		g_signal_connect(recorder, "recorder-started", G_CALLBACK (start_rec), NULL);
		g_signal_connect(recorder, "recorder-paused", G_CALLBACK (pause_rec), NULL);
		g_signal_connect(recorder, "recorder-resumed", G_CALLBACK (resume_rec), NULL);
		g_signal_connect(recorder, "recorder-completed", G_CALLBACK (comp_rec), NULL);
		g_timeout_add(2000, (GSourceFunc) pause_recorder, recorder);
	}

	GMainLoop *loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(loop);

	g_free(location);
	return 0;
}

