#! /bin/bash
# Run this to generate all the initial makefiles, etc.

PROJECT=libgrassmoor

test -d m4 || mkdir m4

autoreconf -v -f --install -Wno-portability || exit $?

if [ -n "$NOCONFIGURE" ]; then
  exit 0
fi

./configure "$@"  && echo "Now type 'make' to compile $PROJECT."
